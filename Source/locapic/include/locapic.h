//
// Created by epw on 2021/10/14.
//

#pragma once

#include <memory>
#include <string>
#include <vector>
#include <functional>

namespace locapic {
    /// Start python interpreter
    /// \param home_id returned python object id
    /// \param python_home directory containing python executable
    /// \return exception text
    std::string init_py_home(std::string &home_id,
                             const std::function<void(std::string)> &add_cpp_task,
                             const std::function<void()> &run_cpp_tasks,
                             const std::function<std::string()> &pop_py_task,
                             const std::function<void(std::string)> &std_out_write = nullptr,
                             const std::function<void(std::string)> &std_err_write = nullptr,
                             const std::string &python_home = {},
                             const std::string &python_path = {});

    /// Synchronously run object function on local python environment
    /// \return exception text
    std::string run_py_tasks();

    /// Evaluate Python expressions and statements
    /// \param expr
    /// \return exception text
    std::string exec(const std::string &expr);
}
