﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <queue>
#include <string>
#include "CoreMinimal.h"
#include "CAsyncTask.h"
#include "CExplorerSubsystem.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogCExplorerSubsystem, All, All);

UENUM(BlueprintType)
enum class ECSubType : uint8
{
	Unknown,
	File,
	Folder,
	Dot,
};

/**
 * 
 */
UCLASS(BlueprintType)
class BYEMBED_API UCSub final : public UObject
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite)
	ECSubType Type = ECSubType::Unknown;

	UPROPERTY(BlueprintReadWrite)
	FString Path;

	UPROPERTY(BlueprintReadWrite)
	FString Name;

	UPROPERTY(BlueprintReadWrite)
	int32 Size;

	UPROPERTY(BlueprintReadWrite)
	FDateTime ModifiedTime;

	UPROPERTY(BlueprintReadWrite)
	FDateTime CreatedTime;

	UPROPERTY(BlueprintReadWrite)
	FText Format;

	UPROPERTY(BlueprintReadWrite)
	FText PathText;

	UPROPERTY(BlueprintReadWrite)
	FText SizeText;

	UPROPERTY(BlueprintReadWrite)
	FText ModifiedTimeText;

	UPROPERTY(BlueprintReadWrite)
	FText CreatedTimeText;

	UFUNCTION(BlueprintCallable)
	UCSub* Set(ECSubType InType, FString InPath, int32 InSize, FDateTime InModifiedTime, FDateTime InCreatedTime);
};

/**
 * 
 */
UCLASS(DisplayName = "Explorer")
class BYEMBED_API UCExplorerSubsystem final : public UGameInstanceSubsystem
{
	GENERATED_BODY()

	DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnUpdate);

	DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnSubs, const TArray<UCSub*>&, Subs);


public:
	static void Log(FString Function, FString Message);
	static TFunction<UCExplorerSubsystem*()> Get;
	virtual void Initialize(FSubsystemCollectionBase& Collection) override;
	virtual void Deinitialize() override;

	UPROPERTY(BlueprintReadWrite)
	FText CurrentDetail;
	static void LogDetail(FText Text);


	// 远程地址
	UPROPERTY(BlueprintReadWrite, BlueprintSetter=SetUrl, BlueprintGetter=GetUrl)
	FString Url;

	UFUNCTION(BlueprintSetter)
	void SetUrl(FString InUrl);

	UFUNCTION(BlueprintGetter)
	FString GetUrl();


	// 当前工作目录
	UPROPERTY(BlueprintReadWrite, BlueprintSetter=SetCwd, BlueprintGetter=GetCwd)
	FString Cwd;

	UFUNCTION(BlueprintSetter)
	void SetCwd(FString Dir);

	UFUNCTION(BlueprintGetter)
	FString GetCwd();

	UPROPERTY(BlueprintAssignable)
	FOnUpdate OnCwd;


	// 当前工作目录跳转
	UFUNCTION(BlueprintCallable, Category = "资源管理器(Explorer)")
	void GoTo();

	UFUNCTION(BlueprintCallable, Category = "资源管理器(Explorer)")
	void GoToSuper();

	UFUNCTION(BlueprintCallable, Category = "资源管理器(Explorer)")
	void GoToSub(UCSub* Sub);


	// 当前工作目录子项
	UPROPERTY(BlueprintReadOnly)
	TArray<UCSub*> SubFolders;

	UPROPERTY(BlueprintReadOnly)
	TArray<UCSub*> SubFiles;

	UPROPERTY(BlueprintAssignable)
	FOnSubs OnSubs;


	// 当前选中子项
	UPROPERTY(BlueprintReadOnly)
	UCSub* SelectedSub = nullptr;

	UFUNCTION(BlueprintCallable, Category = "资源管理器(Explorer)")
	void SelectSub(UCSub* Sub);


	// 子项选项
	UFUNCTION(BlueprintCallable, Category = "资源管理器(Explorer)")
	void Makedir(FString Name);

	UFUNCTION(BlueprintCallable, Category = "资源管理器(Explorer)")
	void RenameSub(UCSub* Sub, FString NewName);

	UFUNCTION(BlueprintCallable, Category = "资源管理器(Explorer)")
	void RemoveSub(UCSub* Sub);
};

UENUM(BlueprintType)
enum class ECCopyType : uint8
{
	Upload,
	Download
};

UCLASS(BlueprintType)
class BYEMBED_API UCCopy final : public UCAsyncTask
{
	GENERATED_BODY()

	std::queue<std::string> Messages;

public:
	UFUNCTION(BlueprintCallable, Category = "拷贝(Copy)")
	void Start(FString ClientParent, FString ServerParent, ECCopyType Type);
};
