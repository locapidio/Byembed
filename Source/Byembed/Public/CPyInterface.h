// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "CPyInterface.generated.h"

class UWidget;
UINTERFACE()
class UCPyLpc : public UInterface
{
	GENERATED_BODY()
};

class BYEMBED_API ICPyLpc
{
	GENERATED_BODY()

public:
	virtual void Run(const FString Method, const TSharedPtr<FJsonObject> In, TSharedPtr<FJsonObject>& Out) =0;
};

UINTERFACE()
class UCPyWidget : public UCPyLpc
{
	GENERATED_BODY()
};

class BYEMBED_API ICPyWidget : public ICPyLpc
{
	GENERATED_BODY()

public:
	virtual UWidget* Init() { return nullptr; }

	virtual void Run(const FString Method, const TSharedPtr<FJsonObject> In, TSharedPtr<FJsonObject>& Out) override;
};
