// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "CGameModeBase.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogCGameModeBase, All, All);

/**
 * 
 */
UCLASS()
class BYEMBED_API ACGameModeBase final : public AGameModeBase
{
	GENERATED_BODY()

	static void Log(const FString Message);

public:
	ACGameModeBase();
};
