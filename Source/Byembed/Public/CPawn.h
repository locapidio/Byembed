// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Camera/CameraComponent.h"
#include "Components/DirectionalLightComponent.h"
#include "GameFramework/Pawn.h"
#include "CPawn.generated.h"

class USphereComponent;

UCLASS(Blueprintable)
class BYEMBED_API ACPawn final : public APawn
{
	GENERATED_BODY()

public:
	ACPawn();

	UPROPERTY(Category = Pawn, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	USceneComponent* Root;

	UPROPERTY(Category = Pawn, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UPawnMovementComponent* Movement;

	UPROPERTY(Category = Pawn, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UDirectionalLightComponent* Light;

	UPROPERTY(Category = Pawn, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UCameraComponent* Camera;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION(BlueprintCallable, Category = "Pawn")
	void MoveForward(const float Val);

	UFUNCTION(BlueprintCallable, Category = "Pawn")
	void MoveRight(const float Val);

	UFUNCTION(BlueprintCallable, Category = "Pawn")
	void MoveUp(const float Val);
};
