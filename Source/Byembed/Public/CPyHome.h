﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <functional>
#include <string>

#include "CoreMinimal.h"
#include "CPyInterface.h"
#include "CMenuAnchor.h"
#include "CSpinBox.h"
#include "Components/BackgroundBlur.h"
#include "Components/Border.h"
#include "Components/Button.h"
#include "Components/CanvasPanel.h"
#include "Components/EditableTextBox.h"
#include "Components/GridPanel.h"
#include "Components/HorizontalBox.h"
#include "Components/Image.h"
#include "Components/MultiLineEditableTextBox.h"
#include "Components/Overlay.h"
#include "Components/ProgressBar.h"
#include "Components/ScaleBox.h"
#include "Components/ScrollBox.h"
#include "Components/SizeBox.h"
#include "Components/Slider.h"
#include "Components/Spacer.h"
#include "Components/TextBlock.h"
#include "Components/UniformGridPanel.h"
#include "Components/VerticalBox.h"
#include "Components/WidgetSwitcher.h"
#include "Components/WrapBox.h"
#include "CPyHome.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogCPyHome, All, All);

UCLASS(BlueprintType)
class BYEMBED_API UCPyHome final : public UCanvasPanel, public ICPyWidget
{
	GENERATED_BODY()

	TArray<TSharedPtr<FJsonObject>> PyTasks;
	TArray<TFunction<void()>> CppTasks;

	bool bEndPython = false;

	std::function<void(std::string)> AddCppTask;
	std::function<void()> RunCppTasks;
	std::function<std::string()> PopPyTask;
	std::function<void(std::string)> StdOut, StdErr;

public:
	static TFunction<UCPyHome*()> Get;

	static void Log(FString Message);

	static TSharedPtr<FJsonObject> JsonFromF(const FString& JsonStr);
	static FString JsonToF(const TSharedPtr<FJsonObject>& JsonObject);

	static TSharedPtr<FJsonObject> JsonFrom(const std::string& JsonStr);
	static std::string JsonTo(const TSharedPtr<FJsonObject>& JsonObject);

	static FVector2D JVector2D(const TArray<TSharedPtr<FJsonValue>>& Json);
	static FLinearColor JLinearColor(const TArray<TSharedPtr<FJsonValue>>& Json);
	static FMargin JMargin(const TArray<TSharedPtr<FJsonValue>>& Json);
	static FAssetData AssetData(const FString Name);
	static FSlateBrush JBrush(const TSharedPtr<FJsonObject> Json);
	static FSlateFontInfo JFont(const TSharedPtr<FJsonObject> Json);

	static void AddPyTask(const UObject* Instance, FString Method, TSharedPtr<FJsonObject> Payload);

	UPROPERTY(BlueprintReadOnly)
	FString PyHomeId;

	UFUNCTION(BlueprintCallable, Category = "CPy")
	void BeginPython(FString PythonHome, FString PythonPath);

	UFUNCTION(BlueprintCallable, Category = "CPy")
	void EndPython();

	virtual void BeginDestroy() override;

	UPROPERTY(BlueprintReadOnly)
	TMap<FString, UObject*> PyInstances;

	UPROPERTY(BlueprintReadOnly)
	TMap<UObject*, FString> PyIds;

	void New(const TArray<FString> Classes, const FString Id);
	void Delete(const UObject* Instance);
	void Delete(const FString Id);

	virtual void Run(const FString Method, const TSharedPtr<FJsonObject> In, TSharedPtr<FJsonObject>& Out) override;
};

// 背景模糊
UCLASS(BlueprintType)
class BYEMBED_API UCPyBackgroundBlur final : public UBackgroundBlur, public ICPyWidget
{
	GENERATED_BODY()

public:
	virtual UCPyBackgroundBlur* Init() override { return this; }

	virtual void Run(const FString Method, const TSharedPtr<FJsonObject> In, TSharedPtr<FJsonObject>& Out) override;
};

// 边界
UCLASS(BlueprintType)
class BYEMBED_API UCPyBorder final : public UBorder, public ICPyWidget
{
	GENERATED_BODY()

public:
	virtual UCPyBorder* Init() override { return this; }

	virtual void Run(const FString Method, const TSharedPtr<FJsonObject> In, TSharedPtr<FJsonObject>& Out) override;
};

// 画布面板
UCLASS(BlueprintType)
class BYEMBED_API UCPyCanvasPanel final : public UCanvasPanel, public ICPyWidget
{
	GENERATED_BODY()

public:
	virtual UCPyCanvasPanel* Init() override { return this; }

	virtual void Run(const FString Method, const TSharedPtr<FJsonObject> In, TSharedPtr<FJsonObject>& Out) override;
};

// 网格面板
UCLASS(BlueprintType)
class BYEMBED_API UCPyGridPanel final : public UGridPanel, public ICPyWidget
{
	GENERATED_BODY()

public:
	virtual UCPyGridPanel* Init() override { return this; }

	virtual void Run(const FString Method, const TSharedPtr<FJsonObject> In, TSharedPtr<FJsonObject>& Out) override;
};

// 水平框
UCLASS(BlueprintType)
class BYEMBED_API UCPyHorizontalBox final : public UHorizontalBox, public ICPyWidget
{
	GENERATED_BODY()

public:
	virtual UCPyHorizontalBox* Init() override { return this; }

	virtual void Run(const FString Method, const TSharedPtr<FJsonObject> In, TSharedPtr<FJsonObject>& Out) override;
};

// 垂直框
UCLASS(BlueprintType)
class BYEMBED_API UCPyVerticalBox final : public UVerticalBox, public ICPyWidget
{
	GENERATED_BODY()

public:
	virtual UCPyVerticalBox* Init() override { return this; }

	virtual void Run(const FString Method, const TSharedPtr<FJsonObject> In, TSharedPtr<FJsonObject>& Out) override;
};

// 覆层
UCLASS(BlueprintType)
class BYEMBED_API UCPyOverlay final : public UOverlay, public ICPyWidget
{
	GENERATED_BODY()

public:
	virtual UCPyOverlay* Init() override { return this; }

	virtual void Run(const FString Method, const TSharedPtr<FJsonObject> In, TSharedPtr<FJsonObject>& Out) override;
};

// 缩放框
UCLASS(BlueprintType)
class BYEMBED_API UCPyScaleBox final : public UScaleBox, public ICPyWidget
{
	GENERATED_BODY()

public:
	virtual UCPyScaleBox* Init() override { return this; }

	virtual void Run(const FString Method, const TSharedPtr<FJsonObject> In, TSharedPtr<FJsonObject>& Out) override;
};

// 滚动框
UCLASS(BlueprintType)
class BYEMBED_API UCPyScrollBox final : public UScrollBox, public ICPyWidget
{
	GENERATED_BODY()

public:
	virtual UCPyScrollBox* Init() override { return this; }

	virtual void Run(const FString Method, const TSharedPtr<FJsonObject> In, TSharedPtr<FJsonObject>& Out) override;
};

// 尺寸框
UCLASS(BlueprintType)
class BYEMBED_API UCPySizeBox final : public USizeBox, public ICPyWidget
{
	GENERATED_BODY()

public:
	virtual UCPySizeBox* Init() override { return this; }

	virtual void Run(const FString Method, const TSharedPtr<FJsonObject> In, TSharedPtr<FJsonObject>& Out) override;
};

// 均匀网格面板
UCLASS(BlueprintType)
class BYEMBED_API UCPyUniformGridPanel final : public UUniformGridPanel, public ICPyWidget
{
	GENERATED_BODY()

public:
	virtual UCPyUniformGridPanel* Init() override { return this; }

	virtual void Run(const FString Method, const TSharedPtr<FJsonObject> In, TSharedPtr<FJsonObject>& Out) override;
};

// 控件切换器
UCLASS(BlueprintType)
class BYEMBED_API UCPyWidgetSwitcher final : public UWidgetSwitcher, public ICPyWidget
{
	GENERATED_BODY()

public:
	virtual UCPyWidgetSwitcher* Init() override { return this; }

	virtual void Run(const FString Method, const TSharedPtr<FJsonObject> In, TSharedPtr<FJsonObject>& Out) override;
};

// 包覆框
UCLASS(BlueprintType)
class BYEMBED_API UCPyWrapBox final : public UWrapBox, public ICPyWidget
{
	GENERATED_BODY()

public:
	virtual UCPyWrapBox* Init() override { return this; }

	virtual void Run(const FString Method, const TSharedPtr<FJsonObject> In, TSharedPtr<FJsonObject>& Out) override;
};

// 菜单锚点
UCLASS(BlueprintType)
class BYEMBED_API UCPyMenuAnchor final : public UCMenuAnchor, public ICPyWidget
{
	GENERATED_BODY()

public:
	virtual UCPyMenuAnchor* Init() override
	{
		OnMenuOpenChanged.AddDynamic(this, &UCPyMenuAnchor::OnMenuOpenChangedEvent);
		return this;
	}

	virtual void Run(const FString Method, const TSharedPtr<FJsonObject> In, TSharedPtr<FJsonObject>& Out) override;

	UFUNCTION()
	void OnMenuOpenChangedEvent(bool bIsOpen);
};

// 按钮
UCLASS(BlueprintType)
class BYEMBED_API UCPyButton final : public UButton, public ICPyWidget
{
	GENERATED_BODY()

public:
	virtual UCPyButton* Init() override
	{
		OnClicked.AddDynamic(this, &UCPyButton::OnClickedEvent);
		OnPressed.AddDynamic(this, &UCPyButton::OnPressedEvent);
		OnReleased.AddDynamic(this, &UCPyButton::OnReleasedEvent);
		OnHovered.AddDynamic(this, &UCPyButton::OnHoveredEvent);
		OnUnhovered.AddDynamic(this, &UCPyButton::OnUnhoveredEvent);
		return this;
	}

	virtual void Run(const FString Method, const TSharedPtr<FJsonObject> In, TSharedPtr<FJsonObject>& Out) override;

	UFUNCTION()
	void OnClickedEvent();
	UFUNCTION()
	void OnPressedEvent();
	UFUNCTION()
	void OnReleasedEvent();
	UFUNCTION()
	void OnHoveredEvent();
	UFUNCTION()
	void OnUnhoveredEvent();
};

// 滑动条
UCLASS(BlueprintType)
class BYEMBED_API UCPySlider final : public USlider, public ICPyWidget
{
	GENERATED_BODY()

public:
	virtual UCPySlider* Init() override
	{
		OnMouseCaptureBegin.AddDynamic(this, &UCPySlider::OnMouseCaptureBeginEvent);
		OnMouseCaptureEnd.AddDynamic(this, &UCPySlider::OnMouseCaptureEndEvent);
		OnControllerCaptureBegin.AddDynamic(this, &UCPySlider::OnControllerCaptureBeginEvent);
		OnControllerCaptureEnd.AddDynamic(this, &UCPySlider::OnControllerCaptureEndEvent);
		OnValueChanged.AddDynamic(this, &UCPySlider::OnValueChangedEvent);
		return this;
	}

	virtual void Run(const FString Method, const TSharedPtr<FJsonObject> In, TSharedPtr<FJsonObject>& Out) override;

	UFUNCTION()
	void OnMouseCaptureBeginEvent();
	UFUNCTION()
	void OnMouseCaptureEndEvent();
	UFUNCTION()
	void OnControllerCaptureBeginEvent();
	UFUNCTION()
	void OnControllerCaptureEndEvent();
	UFUNCTION()
	void OnValueChangedEvent(float InValue);
};

// 进度条
UCLASS(BlueprintType)
class BYEMBED_API UCPyProgressBar final : public UProgressBar, public ICPyWidget
{
	GENERATED_BODY()

public:
	virtual UCPyProgressBar* Init() override { return this; }

	virtual void Run(const FString Method, const TSharedPtr<FJsonObject> In, TSharedPtr<FJsonObject>& Out) override;
};

// 图像
UCLASS(BlueprintType)
class BYEMBED_API UCPyImage final : public UImage, public ICPyWidget
{
	GENERATED_BODY()

public:
	virtual UCPyImage* Init() override { return this; }

	virtual void Run(const FString Method, const TSharedPtr<FJsonObject> In, TSharedPtr<FJsonObject>& Out) override;
};

// 文本块
UCLASS(BlueprintType)
class BYEMBED_API UCPyTextBlock final : public UTextBlock, public ICPyWidget
{
	GENERATED_BODY()

public:
	virtual UCPyTextBlock* Init() override { return this; }

	virtual void Run(const FString Method, const TSharedPtr<FJsonObject> In, TSharedPtr<FJsonObject>& Out) override;
};

// 可编辑文本框
UCLASS(BlueprintType)
class BYEMBED_API UCPySingleLineEditableTextBox final : public UEditableTextBox, public ICPyWidget
{
	GENERATED_BODY()

public:
	virtual UCPySingleLineEditableTextBox* Init() override
	{
		OnTextChanged.AddDynamic(this, &UCPySingleLineEditableTextBox::OnTextChangedEvent);
		OnTextCommitted.AddDynamic(this, &UCPySingleLineEditableTextBox::OnTextCommittedEvent);
		return this;
	}

	virtual void Run(const FString Method, const TSharedPtr<FJsonObject> In, TSharedPtr<FJsonObject>& Out) override;

	UFUNCTION()
	void OnTextChangedEvent(const FText& InText);
	UFUNCTION()
	void OnTextCommittedEvent(const FText& InText, ETextCommit::Type CommitMethod);
};

// 可编辑文本框(多行)
UCLASS(BlueprintType)
class BYEMBED_API UCPyMultiLineEditableTextBox final : public UMultiLineEditableTextBox, public ICPyWidget
{
	GENERATED_BODY()

public:
	virtual UCPyMultiLineEditableTextBox* Init() override
	{
		OnTextChanged.AddDynamic(this, &UCPyMultiLineEditableTextBox::OnTextChangedEvent);
		OnTextCommitted.AddDynamic(this, &UCPyMultiLineEditableTextBox::OnTextCommittedEvent);
		return this;
	}

	virtual void Run(const FString Method, const TSharedPtr<FJsonObject> In, TSharedPtr<FJsonObject>& Out) override;

	UFUNCTION()
	void OnTextChangedEvent(const FText& InText);
	UFUNCTION()
	void OnTextCommittedEvent(const FText& InText, ETextCommit::Type CommitMethod);
};

// 数值框
UCLASS(BlueprintType)
class BYEMBED_API UCPySpinBox final : public UCSpinBox, public ICPyWidget
{
	GENERATED_BODY()

public:
	virtual UCPySpinBox* Init() override
	{
		OnValueChanged.AddDynamic(this, &UCPySpinBox::OnValueChangedEvent);
		OnValueCommitted.AddDynamic(this, &UCPySpinBox::OnValueCommittedEvent);
		OnBeginSliderMovement.AddDynamic(this, &UCPySpinBox::OnBeginSliderMovementEvent);
		OnEndSliderMovement.AddDynamic(this, &UCPySpinBox::OnEndSliderMovementEvent);
		return this;
	}

	virtual void Run(const FString Method, const TSharedPtr<FJsonObject> In, TSharedPtr<FJsonObject>& Out) override;

	UFUNCTION()
	void OnValueChangedEvent(float InValue);
	UFUNCTION()
	void OnValueCommittedEvent(float InValue, ETextCommit::Type CommitMethod);
	UFUNCTION()
	void OnBeginSliderMovementEvent();
	UFUNCTION()
	void OnEndSliderMovementEvent(float InValue);
};

// 间隔区
UCLASS(BlueprintType)
class BYEMBED_API UCPySpacer final : public USpacer, public ICPyWidget
{
	GENERATED_BODY()

public:
	virtual UCPySpacer* Init() override { return this; }

	virtual void Run(const FString Method, const TSharedPtr<FJsonObject> In, TSharedPtr<FJsonObject>& Out) override;
};
