﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Object.h"
#include "CAsyncTask.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogCAsyncTask, All, All);

/**
 * 
 */
UCLASS(BlueprintType)
class BYEMBED_API UCAsyncTask : public UObject
{
	GENERATED_BODY()

	FTimerHandle StatTimer;

public:
	static void Log(const FString Function, FString Message);

	int64 InstantTraffic = 0;
	int64 TotalTraffic = 0;
	int64 EstimatedTraffic = 0;

	UPROPERTY(BlueprintReadWrite)
	bool bCancel = false;

	UPROPERTY(BlueprintReadOnly)
	float ProgressValue;

	UPROPERTY(BlueprintReadOnly)
	FText ProgressText;

	UPROPERTY(BlueprintReadOnly)
	FText NetworkSpeed;

	UPROPERTY(BlueprintReadOnly)
	FDateTime TimeStart;

	UPROPERTY(BlueprintReadOnly)
	FTimespan TimeElapsed;

	UPROPERTY(BlueprintReadOnly, BlueprintGetter=GetCompleted)
	bool bCompleted = false;

	UFUNCTION(BlueprintPure, BlueprintSetter)
	bool GetCompleted() { return bCompleted = ProgressValue == 1; }

	UPROPERTY(BlueprintReadOnly)
	bool bStarted = false;
	void Start();
};
