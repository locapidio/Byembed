// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "CPlayerController.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogCPlayerController, All, All);

/**
 * 
 */
UCLASS(Blueprintable)
class BYEMBED_API ACPlayerController final : public APlayerController
{
	GENERATED_BODY()

	static void Log(const FString Message);
	static void LogTask(const FString Key, const FString Step, const float Progress, const FString Error = FString());

	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnTransferSpeed, FString, SendPerSecond, FString, ReceivePerSecond);

	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnTransferProgress, float, SendProgress, float, ReceiveProgress);

	DECLARE_DYNAMIC_MULTICAST_DELEGATE_FourParams(FOnTask, FText, Name, FString, Step, float, Progress,
	                                              FString, Error);

public:
	ACPlayerController();
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;

	static FString StorageRoot;
	static FString DefaultStorageRoot;

	// 数据包Bytes上限 warning=30523 error=65512
	static constexpr uint32 ChunkSize = 10240;

	template <typename T>
	static TArray<TArray<uint8>> ToChunks(T Content);

	template <typename T>
	static T FromChunks(const TArray<TArray<uint8>>& Chunks);


	FDateTime OneSecTimer = FDateTime::UtcNow();

	FGuid RemoteRoleGuid;

	UPROPERTY(BlueprintReadOnly)
	bool bRemoteServer = false;

	UPROPERTY(BlueprintReadOnly)
	bool bRemoteClient = false;

	UFUNCTION(BlueprintCallable)
	void InitRemoteRole();

	UFUNCTION(Server, Reliable)
	void InitRemoteRole_Server(const FGuid Guid);

	UFUNCTION(Client, Reliable)
	void SetRemoteClient();

	UFUNCTION(Server, Reliable)
	void SetRemoteServer();


	TMap<FString, TArray<TArray<uint8>>> TaskData;

	void TaskDataEmpty();

	UFUNCTION(Server, Reliable)
	void TaskDataEmpty_Server();

	UFUNCTION(Client, Reliable)
	void TaskDataEmpty_Client();

	void TaskDataAdd(const FString& Key, const TArray<uint8>& Chunk);

	UFUNCTION(Server, Reliable)
	void TaskDataAdd_Server(const FString& Key, const TArray<uint8>& Chunk);

	UFUNCTION(Client, Reliable)
	void TaskDataAdd_Client(const FString& Key, const TArray<uint8>& Chunk);


	TMap<FString, int32> TaskDataTransfer;

	UPROPERTY(BlueprintAssignable)
	FOnTransferSpeed OnTransferSpeed;

	UPROPERTY(BlueprintAssignable)
	FOnTransferProgress OnTransferProgress;

	uint32 SendBytesPerSecond = 0, ReceiveBytesPerSecond = 0;
	uint32 SendBytes = 0, ReceiveBytes = 0;
	uint32 TransferSize = 0;

	void InitTransfer(const uint32 Size);

	UFUNCTION(Server, Reliable)
	void InitTransfer_Server(const uint32 Size);

	UFUNCTION(Client, Reliable)
	void InitTransfer_Client(const uint32 Size);

	void AddSendBytes(const uint32 Num);

	UFUNCTION(Server, Reliable)
	void AddSendBytes_Server(const uint32 Num);

	UFUNCTION(Client, Reliable)
	void AddSendBytes_Client(const uint32 Num);

	void AddReceiveBytes(const uint32 Num);

	UFUNCTION(Server, Reliable)
	void AddReceiveBytes_Server(const uint32 Num);

	UFUNCTION(Client, Reliable)
	void AddReceiveBytes_Client(const uint32 Num);


	FText TaskName;
	FString TaskKey;
	FString TaskStep;
	float TaskProgress;
	FString TaskError;

	UPROPERTY(BlueprintAssignable)
	FOnTask OnTask;

	void SetTask(const FText& Name, const FString& Key, const FString& Step, const float Progress,
	             const FString& Error = FString());

	UFUNCTION(Server, Reliable)
	void SetTask_Server(const FText& Name, const FString& Key, const FString& Step, const float Progress,
	                    const FString& Error = FString());

	UFUNCTION(Client, Reliable)
	void SetTask_Client(const FText& Name, const FString& Key, const FString& Step, const float Progress,
	                    const FString& Error = FString());

public:
	// 上传文件
	UFUNCTION(BlueprintCallable, Category = "RPCTask")
	void UploadFile(const FString& Filename, const FString& SavePath);

	void UploadFile();
};
