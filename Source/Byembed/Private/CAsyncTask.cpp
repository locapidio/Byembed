﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "CAsyncTask.h"

#define LOCTEXT_NAMESPACE "CAsyncTask"

DEFINE_LOG_CATEGORY(LogCAsyncTask);

void UCAsyncTask::Log(const FString Function, const FString Message)
{
	UE_LOG(LogCAsyncTask, Display, TEXT("%ls"), *Function);
	UE_LOG(LogCAsyncTask, Display, TEXT("%ls"), *Message);
	GEngine->AddOnScreenDebugMessage(INDEX_NONE, 10, FColor::Orange, Function);
	GEngine->AddOnScreenDebugMessage(INDEX_NONE, 10, FColor::Yellow, Message);
}

void UCAsyncTask::Start()
{
	InstantTraffic = 0;
	TotalTraffic = 0;
	EstimatedTraffic = 0;
	TimeStart = FDateTime::UtcNow();

	GetWorld()->GetTimerManager().SetTimer(StatTimer, [this]
	{
		if (!IsValid(this)) return GetWorld()->GetTimerManager().ClearTimer(StatTimer);
		NetworkSpeed = FText::FromString(FText::AsMemory(InstantTraffic).ToString() + TEXT("/s"));
		TimeElapsed = FDateTime::UtcNow() - TimeStart;
		ProgressValue = EstimatedTraffic > 0 ? TotalTraffic / EstimatedTraffic : 0;
		InstantTraffic = 0;
	}, 1, true);
}

#undef LOCTEXT_NAMESPACE
