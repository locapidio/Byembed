// Fill out your copyright notice in the Description page of Project Settings.


#include "CGameModeBase.h"
#include "CPawn.h"
#include "CPlayerController.h"
#include "GameFramework/GameUserSettings.h"

DEFINE_LOG_CATEGORY(LogCGameModeBase);

void ACGameModeBase::Log(const FString Message)
{
	UE_LOG(LogCGameModeBase, Display, TEXT("%ls"), *Message);
	GEngine->AddOnScreenDebugMessage(INDEX_NONE, 10, FColor::Yellow, Message);
}

ACGameModeBase::ACGameModeBase()
{
	PlayerControllerClass = ACPlayerController::StaticClass();
	DefaultPawnClass = ACPawn::StaticClass();
	bUseSeamlessTravel = false;

	if (GEngine)
	{
		UGameUserSettings* UserSettings = GEngine->GetGameUserSettings();
		UserSettings->SetScreenResolution(FIntPoint(1920, 1080));
		UserSettings->SetFullscreenMode(EWindowMode::Windowed);
		UserSettings->SetVSyncEnabled(true);
		UserSettings->ApplySettings(true);
	}
}
