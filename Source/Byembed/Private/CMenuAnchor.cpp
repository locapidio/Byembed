﻿// Copyright Epic Games, Inc. All Rights Reserved.

#include "CMenuAnchor.h"
#include "Widgets/SNullWidget.h"
#include "Widgets/DeclarativeSyntaxSupport.h"
#include "Widgets/Input/SMenuAnchor.h"
#include "Blueprint/UserWidget.h"

#define LOCTEXT_NAMESPACE "UMG"

/////////////////////////////////////////////////////
// UCMenuAnchor

UCMenuAnchor::UCMenuAnchor(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
	  , ShouldDeferPaintingAfterWindowContent(true)
	  , UseApplicationMenuStack(true)
{
	Placement = MenuPlacement_ComboBox;
	bFitInWindow = true;
}

void UCMenuAnchor::ReleaseSlateResources(bool bReleaseChildren)
{
	Super::ReleaseSlateResources(bReleaseChildren);

	MyMenuAnchor.Reset();
}

TSharedRef<SWidget> UCMenuAnchor::RebuildWidget()
{
	MyMenuAnchor = SNew(SMenuAnchor)
		.Placement(Placement)
		.FitInWindow(bFitInWindow)
		.OnGetMenuContent(BIND_UOBJECT_DELEGATE(FOnGetContent, HandleGetMenuContent))
		.OnMenuOpenChanged(BIND_UOBJECT_DELEGATE(FOnIsOpenChanged, HandleMenuOpenChanged))
		.ShouldDeferPaintingAfterWindowContent(ShouldDeferPaintingAfterWindowContent)
		.UseApplicationMenuStack(UseApplicationMenuStack);

	if (GetChildrenCount() > 0)
	{
		MyMenuAnchor->SetContent(GetContentSlot()->Content
			                         ? GetContentSlot()->Content->TakeWidget()
			                         : SNullWidget::NullWidget);
	}

	return MyMenuAnchor.ToSharedRef();
}

void UCMenuAnchor::OnSlotAdded(UPanelSlot* InSlot)
{
	// Add the child to the live slot if it already exists
	if (MyMenuAnchor.IsValid())
	{
		MyMenuAnchor->SetContent(InSlot->Content ? InSlot->Content->TakeWidget() : SNullWidget::NullWidget);
	}
}

void UCMenuAnchor::OnSlotRemoved(UPanelSlot* InSlot)
{
	// Remove the widget from the live slot if it exists.
	if (MyMenuAnchor.IsValid())
	{
		MyMenuAnchor->SetContent(SNullWidget::NullWidget);
	}
}

void UCMenuAnchor::HandleMenuOpenChanged(bool bIsOpen)
{
	OnMenuOpenChanged.Broadcast(bIsOpen);
}

TSharedRef<SWidget> UCMenuAnchor::HandleGetMenuContent()
{
	TSharedPtr<SWidget> SlateMenuWidget;

	if (MenuWidget)
	{
		SlateMenuWidget = MenuWidget->TakeWidget();
	}

	return SlateMenuWidget.IsValid() ? SlateMenuWidget.ToSharedRef() : SNullWidget::NullWidget;
}

void UCMenuAnchor::ToggleOpen(bool bFocusOnOpen)
{
	if (MyMenuAnchor.IsValid())
	{
		MyMenuAnchor->SetIsOpen(!MyMenuAnchor->IsOpen(), bFocusOnOpen);
	}
}

void UCMenuAnchor::Open(bool bFocusMenu)
{
	if (MyMenuAnchor.IsValid() && !MyMenuAnchor->IsOpen())
	{
		MyMenuAnchor->SetIsOpen(true, bFocusMenu);
	}
}

void UCMenuAnchor::Close()
{
	if (MyMenuAnchor.IsValid())
	{
		return MyMenuAnchor->SetIsOpen(false, false);
	}
}

bool UCMenuAnchor::IsOpen() const
{
	if (MyMenuAnchor.IsValid())
	{
		return MyMenuAnchor->IsOpen();
	}

	return false;
}

void UCMenuAnchor::SetPlacement(TEnumAsByte<EMenuPlacement> InPlacement)
{
	Placement = InPlacement;
	if (MyMenuAnchor.IsValid())
	{
		return MyMenuAnchor->SetMenuPlacement(Placement);
	}
}

void UCMenuAnchor::FitInWindow(bool bFit)
{
	bFitInWindow = bFit;
	if (MyMenuAnchor.IsValid())
	{
		return MyMenuAnchor->SetFitInWindow(bFitInWindow);
	}
}

bool UCMenuAnchor::ShouldOpenDueToClick() const
{
	if (MyMenuAnchor.IsValid())
	{
		return MyMenuAnchor->ShouldOpenDueToClick();
	}

	return false;
}

FVector2D UCMenuAnchor::GetMenuPosition() const
{
	if (MyMenuAnchor.IsValid())
	{
		return MyMenuAnchor->GetMenuPosition();
	}

	return FVector2D(0, 0);
}

bool UCMenuAnchor::HasOpenSubMenus() const
{
	if (MyMenuAnchor.IsValid())
	{
		return MyMenuAnchor->HasOpenSubMenus();
	}

	return false;
}

#if WITH_EDITOR

const FText UCMenuAnchor::GetPaletteCategory()
{
	return LOCTEXT("Primitive", "Primitive");
}

#endif

/////////////////////////////////////////////////////

#undef LOCTEXT_NAMESPACE
