﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "CExplorerSubsystem.h"
#include "CPyHome.h"
#include "locapic.h"

#define LOCTEXT_NAMESPACE "CExplorerSubsystem"

DEFINE_LOG_CATEGORY(LogCExplorerSubsystem);

UCSub* UCSub::Set(const ECSubType InType, const FString InPath, const int32 InSize, const FDateTime InModifiedTime,
                  const FDateTime InCreatedTime)
{
	Type = InType;
	Path = InPath;
	Name = FPaths::GetCleanFilename(Path);
	Size = InSize;
	ModifiedTime = InModifiedTime;
	CreatedTime = InCreatedTime;

	if (Type == ECSubType::Dot)
	{
		Format = LOCTEXT("This\nFolder", "This\nFolder");
	}
	else if (Type == ECSubType::Folder)
	{
		Format = FText();
	}
	else if (Type == ECSubType::File)
	{
		if (Name.EndsWith(TEXT(".dcm")))
		{
			Format = FText::FromString("DICOM");
		}
		else if (Name.EndsWith(TEXT(".nii")) || Name.EndsWith(TEXT(".nii.gz")))
		{
			Format = FText::FromString("NIFTI");
		}
		else if (Name.EndsWith(TEXT(".stl")))
		{
			Format = FText::FromString("STL");
		}
		else Format = FText();
	}

	PathText = FText::FromString(Path);
	SizeText = Type == ECSubType::File ? FText::AsMemory(Size) : FText();
	ModifiedTimeText = FText::FromString(ModifiedTime.ToString(TEXT("%Y-%m-%d %H:%M:%S")));
	CreatedTimeText = FText::FromString(CreatedTime.ToString(TEXT("%Y-%m-%d %H:%M:%S")));
	return this;
}

void UCExplorerSubsystem::Log(const FString Function, const FString Message)
{
	UE_LOG(LogCExplorerSubsystem, Display, TEXT("%ls"), *Function);
	UE_LOG(LogCExplorerSubsystem, Display, TEXT("%ls"), *Message);
	GEngine->AddOnScreenDebugMessage(INDEX_NONE, 10, FColor::Orange, Function);
	GEngine->AddOnScreenDebugMessage(INDEX_NONE, 10, FColor::Yellow, Message);
}

TFunction<UCExplorerSubsystem*()> UCExplorerSubsystem::Get;

void UCExplorerSubsystem::Initialize(FSubsystemCollectionBase& Collection)
{
	Super::Initialize(Collection);
	Get = [this] { return this; };
}

void UCExplorerSubsystem::Deinitialize()
{
	Super::Deinitialize();
}

void UCExplorerSubsystem::LogDetail(const FText Text)
{
	if (Get && Get())
	{
		Get()->CurrentDetail = Text;
		Log(__FUNCTION__, Get()->CurrentDetail.ToString());
	}
}

void UCExplorerSubsystem::SetUrl(FString InUrl)
{
	Url = InUrl;
	GConfig->SetString(TEXT("Explorer"), TEXT("Url"), *Url, GGameUserSettingsIni);
}

FString UCExplorerSubsystem::GetUrl()
{
	GConfig->GetString(TEXT("Explorer"), TEXT("Url"), Url, GGameUserSettingsIni);
	return Url;
}

void UCExplorerSubsystem::SetCwd(FString Dir)
{
	Cwd = Dir;
	GConfig->SetString(TEXT("Explorer"), TEXT("Cwd"), *Cwd, GGameUserSettingsIni);
}

FString UCExplorerSubsystem::GetCwd()
{
	GConfig->GetString(TEXT("Explorer"), TEXT("Cwd"), Cwd, GGameUserSettingsIni);
	return Cwd;
}

void UCExplorerSubsystem::GoTo()
{
	AsyncTask(ENamedThreads::GameThread, [this]
	{
		SelectedSub = nullptr;
		SubFolders.Empty();
		SubFiles.Empty();
		OnSubs.Broadcast({});
	});


	// UCPyHome::AddPyTask([this]()
	// {
	// 	// 查询父目录
	// 	std::string Error;
	// 	Error = locapic::run_rpc(
	// 		TCHAR_TO_UTF8(*Url), "explorer", "stat",
	// 		{
	// 			[this](const std::string&) -> std::string
	// 			{
	// 				const TSharedPtr<FJsonObject> Request = MakeShareable(new FJsonObject());
	// 				Request->SetStringField(TEXT("path"), Cwd);
	// 				Request->SetBoolField(TEXT("enable_sha1"), false);
	// 				return UCPyHome::JsonTo(Request);
	// 			}
	// 		},
	// 		{
	// 			[this](const std::string& Message) -> std::string
	// 			{
	// 				const TSharedPtr<FJsonObject> Response = UCPyHome::JsonFrom(Message);
	// 				if (!Response.IsValid()) return {};
	//
	// 				FString Path;
	// 				Response->TryGetStringField(TEXT("path"), Path);
	//
	// 				int32 Size = 0;
	// 				Response->TryGetNumberField(TEXT("size"), Size);
	//
	// 				double ModifiedTimestamp = 0, CreatedTimestamp = 0;
	// 				Response->TryGetNumberField(TEXT("modified_timestamp"), ModifiedTimestamp);
	// 				Response->TryGetNumberField(TEXT("created_timestamp"), CreatedTimestamp);
	//
	// 				const FDateTime MTime = FDateTime::FromUnixTimestamp(ModifiedTimestamp);
	// 				const FDateTime CTime = FDateTime::FromUnixTimestamp(CreatedTimestamp);
	//
	// 				UCSub* Sub = NewObject<UCSub>(this);
	// 				Sub->Set(ECSubType::Dot, Path, Size, MTime, CTime);
	// 				SubFolders.Add(Sub);
	//
	// 				AsyncTask(ENamedThreads::GameThread, [this]()
	// 				{
	// 					TArray<UCSub*> Subs;
	// 					Subs.Append(SubFolders);
	// 					Subs.Append(SubFiles);
	// 					OnSubs.Broadcast(Subs);
	// 				});
	// 				return {};
	// 			}
	// 		});
	//
	// 	if (!Error.empty())
	// 	{
	// 		Log(__FUNCTION__, UTF8_TO_TCHAR(Error.data()));
	// 		return true;
	// 	}
	//
	// 	// 查询子路径
	// 	Error = locapic::run_rpc(
	// 		TCHAR_TO_UTF8(*Url), "explorer", "listdir",
	// 		{
	// 			[this](const std::string&) -> std::string
	// 			{
	// 				const TSharedPtr<FJsonObject> Request = MakeShareable(new FJsonObject());
	// 				Request->SetStringField(TEXT("path"), Cwd);
	// 				Request->SetBoolField(TEXT("enable_sha1"), false);
	// 				return UCPyHome::JsonTo(Request);
	// 			}
	// 		},
	// 		{
	// 			[this](const std::string& Message) -> std::string
	// 			{
	// 				const TSharedPtr<FJsonObject> Response = UCPyHome::JsonFrom(Message);
	// 				if (!Response.IsValid()) return {};
	//
	// 				FString Path;
	// 				Response->TryGetStringField(TEXT("path"), Path);
	//
	// 				bool bIsDir = false, bIsFile = false;
	// 				Response->TryGetBoolField(TEXT("is_dir"), bIsDir);
	// 				Response->TryGetBoolField(TEXT("is_file"), bIsFile);
	//
	// 				int32 Size = 0;
	// 				Response->TryGetNumberField(TEXT("size"), Size);
	//
	// 				double ModifiedTimestamp = 0, CreatedTimestamp = 0;
	// 				Response->TryGetNumberField(TEXT("modified_timestamp"), ModifiedTimestamp);
	// 				Response->TryGetNumberField(TEXT("created_timestamp"), CreatedTimestamp);
	//
	// 				const FDateTime MTime = FDateTime::FromUnixTimestamp(ModifiedTimestamp);
	// 				const FDateTime CTime = FDateTime::FromUnixTimestamp(CreatedTimestamp);
	//
	// 				if (bIsDir)
	// 				{
	// 					UCSub* Sub = NewObject<UCSub>(this);
	// 					Sub->Set(ECSubType::Folder, Path, Size, MTime, CTime);
	// 					SubFolders.Add(Sub);
	// 				}
	// 				else if (bIsFile)
	// 				{
	// 					UCSub* Sub = NewObject<UCSub>(this);
	// 					Sub->Set(ECSubType::File, Path, Size, MTime, CTime);
	// 					SubFiles.Add(Sub);
	// 				}
	//
	// 				AsyncTask(ENamedThreads::GameThread, [this]()
	// 				{
	// 					TArray<UCSub*> Subs;
	// 					Subs.Append(SubFolders);
	// 					Subs.Append(SubFiles);
	// 					OnSubs.Broadcast(Subs);
	// 				});
	// 				return {};
	// 			}
	// 		});
	//
	// 	if (!Error.empty())
	// 	{
	// 		Log(__FUNCTION__, UTF8_TO_TCHAR(Error.data()));
	// 		return true;
	// 	}
	// 	return true;
	// });
}

void UCExplorerSubsystem::GoToSuper()
{
	SetCwd(FPaths::GetPath(Cwd));
	OnCwd.Broadcast();
	GoTo();
}

void UCExplorerSubsystem::GoToSub(UCSub* Sub)
{
	if (!IsValid(Sub)) return;
	if (Sub->Type == ECSubType::Folder)
	{
		SetCwd(FPaths::Combine(Cwd, Sub->Name));
		OnCwd.Broadcast();
		GoTo();
	}
}

void UCExplorerSubsystem::SelectSub(UCSub* Sub)
{
	SelectedSub = Sub;
}

void UCExplorerSubsystem::Makedir(const FString Name)
{
	const TSharedPtr<FJsonObject> Request = MakeShareable(new FJsonObject());
	Request->SetStringField(TEXT("path"), *FPaths::Combine(Cwd, Name));
	Request->SetBoolField(TEXT("override"), false);
	const std::string Message = UCPyHome::JsonTo(Request);

	// UCPyHome::AddPyTask([this, Message]()
	// {
	// 	const std::string Error = locapic::run_rpc(
	// 		TCHAR_TO_UTF8(*UCExplorerSubsystem::Get()->GetUrl()), "explorer", "makedir",
	// 		{[this, Message](const std::string&) -> std::string { return Message; }},
	// 		{
	// 			[this](const std::string&) -> std::string
	// 			{
	// 				GoTo();
	// 				return {};
	// 			}
	// 		});
	// 	if (!Error.empty())
	// 	{
	// 		Log(__FUNCTION__, UTF8_TO_TCHAR(Error.data()));
	// 		return true;
	// 	}
	// 	return true;
	// });
}

void UCExplorerSubsystem::RenameSub(UCSub* Sub, const FString NewName)
{
	const TSharedPtr<FJsonObject> Request = MakeShareable(new FJsonObject());
	Request->SetStringField(TEXT("path"), *Sub->Path);
	Request->SetStringField(TEXT("new_path"), *FPaths::Combine(FPaths::GetPath(Sub->Path), NewName));
	const std::string Message = UCPyHome::JsonTo(Request);

	// UCPyHome::AddPyTask([this, Message]()
	// {
	// 	const std::string Error = locapic::run_rpc(
	// 		TCHAR_TO_UTF8(*UCExplorerSubsystem::Get()->GetUrl()), "explorer", "rename",
	// 		{[this, Message](const std::string&) -> std::string { return Message; }},
	// 		{
	// 			[this](const std::string&) -> std::string
	// 			{
	// 				GoTo();
	// 				return {};
	// 			}
	// 		});
	// 	if (!Error.empty())
	// 	{
	// 		Log(__FUNCTION__, UTF8_TO_TCHAR(Error.data()));
	// 		return true;
	// 	}
	// 	return true;
	// });
}

void UCExplorerSubsystem::RemoveSub(UCSub* Sub)
{
	const TSharedPtr<FJsonObject> Request = MakeShareable(new FJsonObject());
	Request->SetStringField(TEXT("path"), *Sub->Path);
	const std::string Message = UCPyHome::JsonTo(Request);

	// UCPyHome::AddPyTask([this, Message]()
	// {
	// 	const std::string Error = locapic::run_rpc(
	// 		TCHAR_TO_UTF8(*UCExplorerSubsystem::Get()->GetUrl()), "explorer", "remove",
	// 		{[Message](const std::string&) -> std::string { return Message; }},
	// 		{
	// 			[this](const std::string&) -> std::string
	// 			{
	// 				GoTo();
	// 				return {};
	// 			}
	// 		});
	// 	if (!Error.empty())
	// 	{
	// 		Log(__FUNCTION__, UTF8_TO_TCHAR(Error.data()));
	// 		return true;
	// 	}
	// 	return true;
	// });
}

void UCCopy::Start(FString ClientParent, FString ServerParent, const ECCopyType Type)
{
	Super::Start();

	std::string CopyInit, CopyFile;
	if (Type == ECCopyType::Upload)
	{
		CopyInit = "upload_init";
		CopyFile = "upload_file";
	}
	else if (Type == ECCopyType::Download)
	{
		CopyInit = "download_init";
		CopyFile = "download_file";
	}
	else return;

	// UCPyHome::AddPyTask([this, ClientParent, ServerParent, CopyInit, CopyFile]()
	// {
	// 	std::string Error;
	// 	Error = locapic::run_rpc(
	// 		TCHAR_TO_UTF8(*UCExplorerSubsystem::Get()->GetUrl()), "explorer", CopyInit,
	// 		{
	// 			[this, ClientParent, ServerParent](const std::string&) -> std::string
	// 			{
	// 				const TSharedPtr<FJsonObject> Request = MakeShareable(new FJsonObject());
	// 				Request->SetStringField(TEXT("client_path"), *ClientParent);
	// 				Request->SetStringField(TEXT("server_path"), *ServerParent);
	// 				return UCPyHome::JsonTo(Request);
	// 			}
	// 		},
	// 		{
	// 			[this](const std::string& Message) -> std::string
	// 			{
	// 				int32 Size = 0;
	// 				UCPyHome::JsonFrom(Message)->TryGetNumberField(TEXT("size"), Size);
	// 				EstimatedTraffic += Size;
	// 				Messages.push(Message);
	// 				return {};
	// 			}
	// 		});
	// 	if (!Error.empty())
	// 	{
	// 		Log(__FUNCTION__, UTF8_TO_TCHAR(Error.data()));
	// 		return true;
	// 	}
	//
	// 	Error = locapic::run_rpc(
	// 		TCHAR_TO_UTF8(*UCExplorerSubsystem::Get()->GetUrl()), "explorer", CopyFile,
	// 		{
	// 			[this](const std::string&) -> std::string
	// 			{
	// 				if (!Messages.empty())
	// 				{
	// 					std::string Message = Messages.front();
	// 					Messages.pop();
	// 					return Message;
	// 				}
	// 				return {};
	// 			}
	// 		},
	// 		{
	// 			[this](const std::string& Message) -> std::string
	// 			{
	// 				int32 Size = 0;
	// 				UCPyHome::JsonFrom(Message)->TryGetNumberField(TEXT("size"), Size);
	// 				InstantTraffic += Size;
	// 				TotalTraffic += Size;
	// 				return {};
	// 			}
	// 		});
	// 	if (!Error.empty())
	// 	{
	// 		Log(__FUNCTION__, UTF8_TO_TCHAR(Error.data()));
	// 		return true;
	// 	}
	// 	return true;
	// });
}

#undef LOCTEXT_NAMESPACE
