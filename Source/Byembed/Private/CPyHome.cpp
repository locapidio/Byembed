﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "CPyHome.h"

#include "locapic.h"
#include "Engine/AssetManager.h"
#include "Blueprint/WidgetBlueprintLibrary.h"

DEFINE_LOG_CATEGORY(LogCPyHome);

TFunction<UCPyHome*()> UCPyHome::Get;

void UCPyHome::Log(const FString Message)
{
	UE_LOG(LogCPyHome, Display, TEXT("%ls"), *Message);
	GEngine->AddOnScreenDebugMessage(INDEX_NONE, 10, FColor::Yellow, Message);
}

TSharedPtr<FJsonObject> UCPyHome::JsonFromF(const FString& JsonStr)
{
	TSharedPtr<FJsonObject> JsonObject = nullptr;
	const TSharedRef<TJsonReader<>> JsonReader = TJsonReaderFactory<>::Create(JsonStr);
	FJsonSerializer::Deserialize(JsonReader, JsonObject);
	return JsonObject;
}

FString UCPyHome::JsonToF(const TSharedPtr<FJsonObject>& JsonObject)
{
	FString JsonStr;
	if (JsonObject.IsValid())
	{
		const TSharedRef<TJsonWriter<>> JsonWriter = TJsonWriterFactory<>::Create(&JsonStr);
		FJsonSerializer::Serialize(JsonObject.ToSharedRef(), JsonWriter);
	}
	return JsonStr;
}

TSharedPtr<FJsonObject> UCPyHome::JsonFrom(const std::string& JsonStr)
{
	return JsonFromF(UTF8_TO_TCHAR(JsonStr.data()));
}

std::string UCPyHome::JsonTo(const TSharedPtr<FJsonObject>& JsonObject)
{
	return TCHAR_TO_UTF8(*JsonToF(JsonObject));
}

FVector2D UCPyHome::JVector2D(const TArray<TSharedPtr<FJsonValue>>& Json)
{
	return FVector2D(Json[0]->AsNumber(), Json[1]->AsNumber());
}

FLinearColor UCPyHome::JLinearColor(const TArray<TSharedPtr<FJsonValue>>& Json)
{
	return FLinearColor(Json[0]->AsNumber(), Json[1]->AsNumber(), Json[2]->AsNumber(), Json[3]->AsNumber());
}

FMargin UCPyHome::JMargin(const TArray<TSharedPtr<FJsonValue>>& Json)
{
	return FMargin(Json[0]->AsNumber(), Json[1]->AsNumber(), Json[2]->AsNumber(), Json[3]->AsNumber());
}

FAssetData UCPyHome::AssetData(const FString Name)
{
	const FString Path = FString::Printf(TEXT("/Byembed/Share/%ls.%ls"), *Name, *Name);
	return UAssetManager::Get().GetAssetRegistry().GetAssetByObjectPath(FName(*Path));
}

FSlateBrush UCPyHome::JBrush(const TSharedPtr<FJsonObject> Json)
{
	const int32 Width = Json->GetIntegerField(TEXT("width"));
	const int32 Height = Json->GetIntegerField(TEXT("height"));
	const FString Asset = Json->GetStringField(TEXT("asset"));

	UTexture2D* Texture = Cast<UTexture2D>(AssetData(Asset).GetAsset());
	UMaterialInterface* Material = Cast<UMaterialInterface>(AssetData(Asset).GetAsset());

	FSlateBrush Brush;
	if (Texture) Brush = UWidgetBlueprintLibrary::MakeBrushFromTexture(Texture, Width, Height);
	if (Material) Brush = UWidgetBlueprintLibrary::MakeBrushFromMaterial(Material, Width, Height);

	Brush.TintColor = JLinearColor(Json->GetArrayField(TEXT("color")));
	Brush.Margin = FMargin(0);
	return Brush;
}

FSlateFontInfo UCPyHome::JFont(const TSharedPtr<FJsonObject> Json)
{
	FSlateFontInfo Font;
	Font.FontObject = AssetData(Json->GetStringField(TEXT("font_object"))).GetAsset();
	Font.TypefaceFontName = FName(*Json->GetStringField(TEXT("typeface_font_name")));
	Font.Size = Json->GetIntegerField(TEXT("size"));
	Font.LetterSpacing = Json->GetIntegerField(TEXT("letter_spacing"));
	Font.FontMaterial = AssetData(Json->GetStringField(TEXT("font_material"))).GetAsset();

	const TSharedPtr<FJsonObject>& Outline = Json->GetObjectField(TEXT("outline"));
	Font.OutlineSettings.OutlineSize = Outline->GetIntegerField(TEXT("outline_size"));
	Font.OutlineSettings.OutlineColor = JLinearColor(Outline->GetArrayField(TEXT("outline_color")));
	Font.OutlineSettings.OutlineMaterial = AssetData(Outline->GetStringField(TEXT("outline_material"))).GetAsset();
	return Font;
}

void UCPyHome::AddPyTask(const UObject* Instance, const FString Method, const TSharedPtr<FJsonObject> Payload)
{
	if (!Get()->PyIds.Contains(Instance)) return;

	const TSharedPtr<FJsonObject> Message = MakeShareable(new FJsonObject());
	Message->SetStringField(TEXT("instance"), Get()->PyIds[Instance]);
	Message->SetStringField(TEXT("method"), Method);
	Message->SetObjectField(TEXT("payload"), Payload.IsValid() ? Payload : MakeShareable(new FJsonObject()));
	Get()->PyTasks.Add(Message);
}

void UCPyHome::BeginPython(const FString PythonHome, const FString PythonPath)
{
	Get = [this] { return this; };

	AddCppTask = [this](const std::string& Message)
	{
		if (Message.empty())
		{
			return Log(TEXT("Ignore empty message"));
		}

		if (const TSharedPtr<FJsonObject> In = JsonFrom(Message))
		{
			const FString InstanceId = In->GetStringField(TEXT("instance"));
			const FString Method = In->GetStringField(TEXT("method"));

			Get()->CppTasks.Add([this, InstanceId, Method, In]
			{
				if (!PyInstances.Contains(InstanceId))
				{
					return Log(FString::Printf(TEXT("Ignore unknown instance %ls %ls"), *InstanceId, *Method));
				}
				if (!IsValid(PyInstances[InstanceId]))
				{
					return Log(FString::Printf(TEXT("Ignore invalid instance %ls %ls"), *InstanceId, *Method));
				}
				if (ICPyLpc* Lpc = Cast<ICPyLpc>(PyInstances[InstanceId]))
				{
					TSharedPtr<FJsonObject> Out = nullptr;
					return Lpc->Run(Method, In, Out);
				}
				return Log(FString::Printf(TEXT("Ignore unimplemented interface %ls %ls"), *InstanceId, *Method));
			});
			return;
		}
		return Log(FString::Printf(TEXT("Ignore unresolved message %hs"), Message.data()));
	};

	PopPyTask = [this]
	{
		if (PyTasks.Num() > 0)
		{
			std::string Message = JsonTo(PyTasks[0]);
			PyTasks.RemoveAt(0);
			return Message;
		}
		return std::string{};
	};

	RunCppTasks = [this]
	{
		if (IsInGameThread())
		{
			for (const auto& Task : CppTasks) Task();
			CppTasks.Empty();
		}
		else
		{
			FScopedEvent L;
			AsyncTask(ENamedThreads::GameThread, [this, &L]
			{
				for (const auto& Task : CppTasks) Task();
				CppTasks.Empty();
				L.Trigger();
			});
		}
	};

	StdOut = [](const std::string& Message)
	{
		UE_LOG(LogCPyHome, Display, TEXT("%ls"), UTF8_TO_TCHAR(Message.data()));
		GEngine->AddOnScreenDebugMessage(INDEX_NONE, 10, FColor::Yellow, UTF8_TO_TCHAR(Message.data()));
	};

	StdErr = [](const std::string& Message)
	{
		UE_LOG(LogCPyHome, Error, TEXT("%ls"), UTF8_TO_TCHAR(Message.data()));
		GEngine->AddOnScreenDebugMessage(INDEX_NONE, 10, FColor::Red, UTF8_TO_TCHAR(Message.data()));
	};

	AsyncTask(ENamedThreads::AnyBackgroundThreadNormalTask, [this, PythonHome, PythonPath]
	{
		std::string Id;
		const auto Error = locapic::init_py_home(Id, AddCppTask, RunCppTasks, PopPyTask, StdOut, StdErr,
		                                         TCHAR_TO_UTF8(*PythonHome), TCHAR_TO_UTF8(*PythonPath));

		if (Error.empty()) StdOut("py_init succeeded " + Id);
		else return StdErr("py_init failed " + Error);

		PyHomeId = UTF8_TO_TCHAR(Id.data());
		PyInstances.Add(PyHomeId, this);
		PyIds.Add(this, PyHomeId);

		while (!bEndPython)
		{
			locapic::run_py_tasks();
			RunCppTasks();
		}
	});
}

void UCPyHome::EndPython()
{
	bEndPython = true;
}

void UCPyHome::BeginDestroy()
{
	bEndPython = true;
	Super::BeginDestroy();
}

void UCPyHome::New(const TArray<FString> Classes, const FString Id)
{
	UObject* Instance = nullptr;

	for (const FString Class : Classes)
	{
		if (Class == TEXT("UBackgroundBlur"))
		{
			Instance = NewObject<UCPyBackgroundBlur>()->Init();
			break;
		}
		if (Class == TEXT("UBorder"))
		{
			Instance = NewObject<UCPyBorder>()->Init();
			break;
		}
		if (Class == TEXT("UCanvasPanel"))
		{
			Instance = NewObject<UCPyCanvasPanel>()->Init();
			break;
		}
		if (Class == TEXT("UGridPanel"))
		{
			Instance = NewObject<UCPyGridPanel>()->Init();
			break;
		}
		if (Class == TEXT("UHorizontalBox"))
		{
			Instance = NewObject<UCPyHorizontalBox>()->Init();
			break;
		}
		if (Class == TEXT("UVerticalBox"))
		{
			Instance = NewObject<UCPyVerticalBox>()->Init();
			break;
		}
		if (Class == TEXT("UOverlay"))
		{
			Instance = NewObject<UCPyOverlay>()->Init();
			break;
		}
		if (Class == TEXT("UScaleBox"))
		{
			Instance = NewObject<UCPyScaleBox>()->Init();
			break;
		}
		if (Class == TEXT("UScrollBox"))
		{
			Instance = NewObject<UCPyScrollBox>()->Init();
			break;
		}
		if (Class == TEXT("USizeBox"))
		{
			Instance = NewObject<UCPySizeBox>()->Init();
			break;
		}
		if (Class == TEXT("UUniformGridPanel"))
		{
			Instance = NewObject<UCPyUniformGridPanel>()->Init();
			break;
		}
		if (Class == TEXT("UWidgetSwitcher"))
		{
			Instance = NewObject<UCPyWidgetSwitcher>()->Init();
			break;
		}
		if (Class == TEXT("UWrapBox"))
		{
			Instance = NewObject<UCPyWrapBox>()->Init();
			break;
		}
		if (Class == TEXT("UMenuAnchor"))
		{
			Instance = NewObject<UCPyMenuAnchor>()->Init();
			break;
		}
		if (Class == TEXT("UButton"))
		{
			Instance = NewObject<UCPyButton>()->Init();
			break;
		}
		if (Class == TEXT("USlider"))
		{
			Instance = NewObject<UCPySlider>()->Init();
			break;
		}
		if (Class == TEXT("UProgressBar"))
		{
			Instance = NewObject<UCPyProgressBar>()->Init();
			break;
		}
		if (Class == TEXT("UImage"))
		{
			Instance = NewObject<UCPyImage>()->Init();
			break;
		}
		if (Class == TEXT("UTextBlock"))
		{
			Instance = NewObject<UCPyTextBlock>()->Init();
			break;
		}
		if (Class == TEXT("USingleLineEditableTextBox"))
		{
			Instance = NewObject<UCPySingleLineEditableTextBox>()->Init();
			break;
		}
		if (Class == TEXT("UMultiLineEditableTextBox"))
		{
			Instance = NewObject<UCPyMultiLineEditableTextBox>()->Init();
			break;
		}
		if (Class == TEXT("USpinBox"))
		{
			Instance = NewObject<UCPySpinBox>()->Init();
			break;
		}
		if (Class == TEXT("USpacer"))
		{
			Instance = NewObject<UCPySpacer>()->Init();
			break;
		}
	}

	if (IsValid(Instance))
	{
		PyInstances.Add(Id, Instance);
		PyIds.Add(Instance, Id);
	}
	else Log(FString(__FUNCTION__) + TEXT(" invalid class ") + Classes[0]);
}

void UCPyHome::Delete(const UObject* Instance)
{
	if (PyIds.Contains(Instance))
	{
		PyInstances.Remove(PyIds[Instance]);
	}
	PyIds.Remove(Instance);
}

void UCPyHome::Delete(const FString Id)
{
	if (PyInstances.Contains(Id))
	{
		PyIds.Remove(PyInstances[Id]);
	}
	PyInstances.Remove(Id);
}

void UCPyHome::Run(const FString Method, const TSharedPtr<FJsonObject> In, TSharedPtr<FJsonObject>& Out)
{
	if (Method == TEXT("print_message"))
	{
		const FString Message = In->GetStringField(TEXT("payload"));
		Log(Message);
	}
	if (Method == TEXT("new"))
	{
		TArray<FString> Classes;
		In->GetObjectField(TEXT("payload"))->TryGetStringArrayField(TEXT("classes"), Classes);
		const FString Id = In->GetObjectField(TEXT("payload"))->GetStringField(TEXT("id"));
		New(Classes, Id);
	}
	return ICPyWidget::Run(Method, In, Out);
}

void UCPyBackgroundBlur::Run(const FString Method, const TSharedPtr<FJsonObject> In, TSharedPtr<FJsonObject>& Out)
{
	if (Method == TEXT("blur_strength"))
	{
		SetBlurStrength(In->GetNumberField(TEXT("payload")));
	}
	return ICPyWidget::Run(Method, In, Out);
}

void UCPyBorder::Run(const FString Method, const TSharedPtr<FJsonObject> In, TSharedPtr<FJsonObject>& Out)
{
	if (Method == TEXT("brush"))
	{
		SetBrush(UCPyHome::JBrush(In->GetObjectField(TEXT("payload"))));
	}
	return ICPyWidget::Run(Method, In, Out);
}

void UCPyCanvasPanel::Run(const FString Method, const TSharedPtr<FJsonObject> In, TSharedPtr<FJsonObject>& Out)
{
	return ICPyWidget::Run(Method, In, Out);
}

void UCPyGridPanel::Run(const FString Method, const TSharedPtr<FJsonObject> In, TSharedPtr<FJsonObject>& Out)
{
	if (Method == TEXT("grid_rules"))
	{
		const TArray<TSharedPtr<FJsonValue>>& Row = In->GetArrayField(TEXT("payload"))[0]->AsArray();
		for (int32 i = 0; i < Row.Num(); ++i)
		{
			SetRowFill(i, Row[i]->AsNumber());
		}
		const TArray<TSharedPtr<FJsonValue>>& Column = In->GetArrayField(TEXT("payload"))[1]->AsArray();
		for (int32 i = 0; i < Column.Num(); ++i)
		{
			SetColumnFill(i, Column[i]->AsNumber());
		}
	}
	return ICPyWidget::Run(Method, In, Out);
}

void UCPyHorizontalBox::Run(const FString Method, const TSharedPtr<FJsonObject> In, TSharedPtr<FJsonObject>& Out)
{
	return ICPyWidget::Run(Method, In, Out);
}

void UCPyVerticalBox::Run(const FString Method, const TSharedPtr<FJsonObject> In, TSharedPtr<FJsonObject>& Out)
{
	return ICPyWidget::Run(Method, In, Out);
}

void UCPyOverlay::Run(const FString Method, const TSharedPtr<FJsonObject> In, TSharedPtr<FJsonObject>& Out)
{
	return ICPyWidget::Run(Method, In, Out);
}

void UCPyScaleBox::Run(const FString Method, const TSharedPtr<FJsonObject> In, TSharedPtr<FJsonObject>& Out)
{
	if (Method == TEXT("scale_rule"))
	{
		const FString String = In->GetStringField(TEXT("payload"));
		EStretch::Type Enum = EStretch::None;
		if (String == TEXT("Fill")) Enum = EStretch::Fill;
		if (String == TEXT("ScaleToFit")) Enum = EStretch::ScaleToFit;
		if (String == TEXT("ScaleToFitX")) Enum = EStretch::ScaleToFitX;
		if (String == TEXT("ScaleToFitY")) Enum = EStretch::ScaleToFitY;
		if (String == TEXT("ScaleToFill")) Enum = EStretch::ScaleToFill;
		if (String == TEXT("ScaleBySafeZone")) Enum = EStretch::ScaleBySafeZone;
		if (String == TEXT("UserSpecified")) Enum = EStretch::UserSpecified;
		SetStretch(Enum);
	}
	if (Method == TEXT("scale_direction"))
	{
		const FString String = In->GetStringField(TEXT("payload"));
		EStretchDirection::Type Enum = EStretchDirection::Both;
		if (String == TEXT("DownOnly")) Enum = EStretchDirection::DownOnly;
		if (String == TEXT("UpOnly")) Enum = EStretchDirection::UpOnly;
		SetStretchDirection(Enum);
	}
	if (Method == TEXT("scale_value"))
	{
		SetUserSpecifiedScale(In->GetNumberField(TEXT("payload")));
	}
	if (Method == TEXT("scale_ignore_inherited"))
	{
		SetIgnoreInheritedScale(In->GetBoolField(TEXT("payload")));
	}
	return ICPyWidget::Run(Method, In, Out);
}

void UCPyScrollBox::Run(const FString Method, const TSharedPtr<FJsonObject> In, TSharedPtr<FJsonObject>& Out)
{
	if (Method == TEXT("box_style"))
	{
		const TSharedPtr<FJsonObject>& Payload = In->GetObjectField(TEXT("payload"));

		FScrollBoxStyle Style;
		Style.SetLeftShadowBrush(UCPyHome::JBrush(Payload->GetObjectField(TEXT("left_shadow"))));
		Style.SetRightShadowBrush(UCPyHome::JBrush(Payload->GetObjectField(TEXT("right_shadow"))));
		Style.SetTopShadowBrush(UCPyHome::JBrush(Payload->GetObjectField(TEXT("top_shadow"))));
		Style.SetBottomShadowBrush(UCPyHome::JBrush(Payload->GetObjectField(TEXT("bottom_shadow"))));
		WidgetStyle = Style;
		SynchronizeProperties();
	}
	if (Method == TEXT("bar_style"))
	{
		const TSharedPtr<FJsonObject>& Payload = In->GetObjectField(TEXT("payload"));

		FScrollBarStyle Style;
		Style.SetHorizontalBackgroundImage(UCPyHome::JBrush(Payload->GetObjectField(TEXT("horizontal_background"))));
		Style.SetHorizontalTopSlotImage(UCPyHome::JBrush(Payload->GetObjectField(TEXT("horizontal_top_slot"))));
		Style.SetHorizontalBottomSlotImage(UCPyHome::JBrush(Payload->GetObjectField(TEXT("horizontal_bottom_slot"))));

		Style.SetVerticalBackgroundImage(UCPyHome::JBrush(Payload->GetObjectField(TEXT("vertical_background"))));
		Style.SetVerticalTopSlotImage(UCPyHome::JBrush(Payload->GetObjectField(TEXT("vertical_top_slot"))));
		Style.SetVerticalBottomSlotImage(UCPyHome::JBrush(Payload->GetObjectField(TEXT("vertical_bottom_slot"))));

		Style.SetNormalThumbImage(UCPyHome::JBrush(Payload->GetObjectField(TEXT("normal_thumb"))));
		Style.SetHoveredThumbImage(UCPyHome::JBrush(Payload->GetObjectField(TEXT("hovered_thumb"))));
		Style.SetDraggedThumbImage(UCPyHome::JBrush(Payload->GetObjectField(TEXT("dragged_thumb"))));

		WidgetBarStyle = Style;
		SynchronizeProperties();
	}
	if (Method == TEXT("consume_mouse_wheel"))
	{
		const FString String = In->GetStringField(TEXT("payload"));
		EConsumeMouseWheel Enum = EConsumeMouseWheel::WhenScrollingPossible;
		if (String == TEXT("Always")) Enum = EConsumeMouseWheel::Always;
		if (String == TEXT("Never")) Enum = EConsumeMouseWheel::Never;
		SetConsumeMouseWheel(Enum);
	}
	if (Method == TEXT("orientation"))
	{
		const FString String = In->GetStringField(TEXT("payload"));
		EOrientation Enum = Orient_Vertical;
		if (String == TEXT("Orient_Horizontal")) Enum = Orient_Horizontal;
		SetOrientation(Enum);
	}
	if (Method == TEXT("scrollbar_visibility"))
	{
		const FString String = In->GetStringField(TEXT("payload"));
		ESlateVisibility Enum = ESlateVisibility::Visible;
		if (String == TEXT("Collapsed")) Enum = ESlateVisibility::Collapsed;
		if (String == TEXT("Hidden")) Enum = ESlateVisibility::Hidden;
		if (String == TEXT("HitTestInvisible")) Enum = ESlateVisibility::HitTestInvisible;
		if (String == TEXT("SelfHitTestInvisible")) Enum = ESlateVisibility::SelfHitTestInvisible;
		SetScrollBarVisibility(Enum);
	}
	if (Method == TEXT("scrollbar_thickness"))
	{
		SetScrollbarThickness(UCPyHome::JVector2D(In->GetArrayField(TEXT("payload"))));
	}
	if (Method == TEXT("scrollbar_padding"))
	{
		SetScrollbarPadding(UCPyHome::JMargin(In->GetArrayField(TEXT("payload"))));
	}
	if (Method == TEXT("always_show_scrollbar"))
	{
		SetAlwaysShowScrollbar(In->GetBoolField(TEXT("payload")));
	}
	if (Method == TEXT("allow_overscroll"))
	{
		SetAllowOverscroll(In->GetBoolField(TEXT("payload")));
	}
	if (Method == TEXT("animate_wheel_scrolling"))
	{
		SetAnimateWheelScrolling(In->GetBoolField(TEXT("payload")));
	}
	if (Method == TEXT("wheel_scroll_multiplier"))
	{
		SetWheelScrollMultiplier(In->GetNumberField(TEXT("payload")));
	}
	if (Method == TEXT("scroll_when_focus_changes"))
	{
		const FString String = In->GetStringField(TEXT("payload"));
		EScrollWhenFocusChanges Enum = EScrollWhenFocusChanges::NoScroll;
		if (String == TEXT("InstantScroll")) Enum = EScrollWhenFocusChanges::InstantScroll;
		if (String == TEXT("AnimatedScroll")) Enum = EScrollWhenFocusChanges::AnimatedScroll;
		SetScrollWhenFocusChanges(Enum);
	}
	return ICPyWidget::Run(Method, In, Out);
}

void UCPySizeBox::Run(const FString Method, const TSharedPtr<FJsonObject> In, TSharedPtr<FJsonObject>& Out)
{
	if (Method == TEXT("width_override"))
	{
		bOverride_WidthOverride = In->GetArrayField(TEXT("payload"))[0]->AsBool();
		SetWidthOverride(In->GetArrayField(TEXT("payload"))[1]->AsNumber());
	}
	if (Method == TEXT("height_override"))
	{
		bOverride_HeightOverride = In->GetArrayField(TEXT("payload"))[0]->AsBool();
		SetHeightOverride(In->GetArrayField(TEXT("payload"))[1]->AsNumber());
	}
	if (Method == TEXT("min_desired_width"))
	{
		bOverride_MinDesiredWidth = In->GetArrayField(TEXT("payload"))[0]->AsBool();
		SetMinDesiredWidth(In->GetArrayField(TEXT("payload"))[1]->AsNumber());
	}
	if (Method == TEXT("min_desired_height"))
	{
		bOverride_MinDesiredHeight = In->GetArrayField(TEXT("payload"))[0]->AsBool();
		SetMinDesiredHeight(In->GetArrayField(TEXT("payload"))[1]->AsNumber());
	}
	if (Method == TEXT("max_desired_width"))
	{
		bOverride_MaxDesiredWidth = In->GetArrayField(TEXT("payload"))[0]->AsBool();
		SetMaxDesiredWidth(In->GetArrayField(TEXT("payload"))[1]->AsNumber());
	}
	if (Method == TEXT("max_desired_height"))
	{
		bOverride_MaxDesiredHeight = In->GetArrayField(TEXT("payload"))[0]->AsBool();
		SetMaxDesiredHeight(In->GetArrayField(TEXT("payload"))[1]->AsNumber());
	}
	if (Method == TEXT("min_aspect_ratio"))
	{
		bOverride_MinAspectRatio = In->GetArrayField(TEXT("payload"))[0]->AsBool();
		SetMinAspectRatio(In->GetArrayField(TEXT("payload"))[1]->AsNumber());
	}
	if (Method == TEXT("max_aspect_ratio"))
	{
		bOverride_MaxAspectRatio = In->GetArrayField(TEXT("payload"))[0]->AsBool();
		SetMaxAspectRatio(In->GetArrayField(TEXT("payload"))[1]->AsNumber());
	}
	return ICPyWidget::Run(Method, In, Out);
}

void UCPyUniformGridPanel::Run(const FString Method, const TSharedPtr<FJsonObject> In, TSharedPtr<FJsonObject>& Out)
{
	return ICPyWidget::Run(Method, In, Out);
}

void UCPyWidgetSwitcher::Run(const FString Method, const TSharedPtr<FJsonObject> In, TSharedPtr<FJsonObject>& Out)
{
	if (Method == TEXT("active_widget"))
	{
		const FString Id = In->GetStringField(TEXT("payload"));
		if (UCPyHome::Get()->PyInstances.Contains(Id))
		{
			if (UWidget* Widget = Cast<UWidget>(UCPyHome::Get()->PyInstances[Id]))
			{
				SetActiveWidget(Widget);
			}
		}
	}
	if (Method == TEXT("active_widget_index"))
	{
		SetActiveWidgetIndex(In->GetIntegerField(TEXT("payload")));
	}
	return ICPyWidget::Run(Method, In, Out);
}

void UCPyWrapBox::Run(const FString Method, const TSharedPtr<FJsonObject> In, TSharedPtr<FJsonObject>& Out)
{
	if (Method == TEXT("inner_slot_padding"))
	{
		SetInnerSlotPadding(UCPyHome::JVector2D(In->GetArrayField(TEXT("payload"))));
	}
	if (Method == TEXT("wrap_size"))
	{
		bExplicitWrapSize = In->GetArrayField(TEXT("payload"))[0]->AsBool();
		WrapSize = In->GetArrayField(TEXT("payload"))[1]->AsNumber();
	}
	if (Method == TEXT("orientation"))
	{
		const FString String = In->GetStringField(TEXT("payload"));
		EOrientation Enum = Orient_Horizontal;
		if (String == TEXT("Orient_Vertical")) Enum = Orient_Vertical;
		Orientation = Enum;
	}
	return ICPyWidget::Run(Method, In, Out);
}

void UCPyMenuAnchor::Run(const FString Method, const TSharedPtr<FJsonObject> In, TSharedPtr<FJsonObject>& Out)
{
	if (Method == TEXT("menu_widget"))
	{
		const FString Id = In->GetStringField(TEXT("payload"));
		if (UCPyHome::Get()->PyInstances.Contains(Id))
		{
			if (UWidget* Widget = Cast<UWidget>(UCPyHome::Get()->PyInstances[Id]))
			{
				MenuWidget = Widget;
			}
		}
		MenuWidget = nullptr;
	}
	if (Method == TEXT("fit_in_window"))
	{
		FitInWindow(In->GetBoolField(TEXT("payload")));
	}
	if (Method == TEXT("placement"))
	{
		const FString String = In->GetStringField(TEXT("payload"));
		EMenuPlacement Enum = MenuPlacement_ComboBox;
		if (String == TEXT("BelowAnchor")) Enum = MenuPlacement_BelowAnchor;
		if (String == TEXT("CenteredBelowAnchor")) Enum = MenuPlacement_CenteredBelowAnchor;
		if (String == TEXT("BelowRightAnchor")) Enum = MenuPlacement_BelowRightAnchor;
		if (String == TEXT("ComboBoxRight")) Enum = MenuPlacement_ComboBoxRight;
		if (String == TEXT("MenuRight")) Enum = MenuPlacement_MenuRight;
		if (String == TEXT("AboveAnchor")) Enum = MenuPlacement_AboveAnchor;
		if (String == TEXT("CenteredAboveAnchor")) Enum = MenuPlacement_CenteredAboveAnchor;
		if (String == TEXT("AboveRightAnchor")) Enum = MenuPlacement_AboveRightAnchor;
		if (String == TEXT("MenuLeft")) Enum = MenuPlacement_MenuLeft;
		if (String == TEXT("Center")) Enum = MenuPlacement_Center;
		if (String == TEXT("RightLeftCenter")) Enum = MenuPlacement_RightLeftCenter;
		if (String == TEXT("MatchBottomLeft")) Enum = MenuPlacement_MatchBottomLeft;
		SetPlacement(Enum);
	}
	if (Method == TEXT("open"))
	{
		Open(true);
	}
	if (Method == TEXT("close"))
	{
		Close();
	}
	if (Method == TEXT("toggle_open"))
	{
		ToggleOpen(true);
	}
	return ICPyWidget::Run(Method, In, Out);
}

// ReSharper disable once CppMemberFunctionMayBeConst
void UCPyMenuAnchor::OnMenuOpenChangedEvent(const bool bIsOpen)
{
	const TSharedPtr<FJsonObject> Payload = MakeShareable(new FJsonObject());
	Payload->SetBoolField(TEXT("is_open"), bIsOpen);
	UCPyHome::AddPyTask(this, TEXT("on_menu_open_changed"), Payload);
}

void UCPyButton::Run(const FString Method, const TSharedPtr<FJsonObject> In, TSharedPtr<FJsonObject>& Out)
{
	if (Method == TEXT("style"))
	{
		const TSharedPtr<FJsonObject>& Payload = In->GetObjectField(TEXT("payload"));
		FButtonStyle Style;
		Style.SetNormal(UCPyHome::JBrush(Payload->GetObjectField(TEXT("normal"))));
		Style.SetHovered(UCPyHome::JBrush(Payload->GetObjectField(TEXT("hovered"))));
		Style.SetPressed(UCPyHome::JBrush(Payload->GetObjectField(TEXT("pressed"))));
		Style.SetDisabled(UCPyHome::JBrush(Payload->GetObjectField(TEXT("disabled"))));
		SetStyle(Style);
	}
	return ICPyWidget::Run(Method, In, Out);
}

// ReSharper disable once CppMemberFunctionMayBeConst
void UCPyButton::OnClickedEvent()
{
	UCPyHome::AddPyTask(this, TEXT("on_clicked"), nullptr);
}

// ReSharper disable once CppMemberFunctionMayBeConst
void UCPyButton::OnPressedEvent()
{
	UCPyHome::AddPyTask(this, TEXT("on_pressed"), nullptr);
}

// ReSharper disable once CppMemberFunctionMayBeConst
void UCPyButton::OnReleasedEvent()
{
	UCPyHome::AddPyTask(this, TEXT("on_released"), nullptr);
}

// ReSharper disable once CppMemberFunctionMayBeConst
void UCPyButton::OnHoveredEvent()
{
	UCPyHome::AddPyTask(this, TEXT("on_hovered"), nullptr);
}

// ReSharper disable once CppMemberFunctionMayBeConst
void UCPyButton::OnUnhoveredEvent()
{
	UCPyHome::AddPyTask(this, TEXT("on_unhovered"), nullptr);
}

void UCPySlider::Run(const FString Method, const TSharedPtr<FJsonObject> In, TSharedPtr<FJsonObject>& Out)
{
	if (Method == TEXT("style"))
	{
		const TSharedPtr<FJsonObject>& Payload = In->GetObjectField(TEXT("payload"));
		FSliderStyle Style;
		Style.SetNormalBarImage(UCPyHome::JBrush(Payload->GetObjectField(TEXT("normal_bar"))));
		Style.SetHoveredBarImage(UCPyHome::JBrush(Payload->GetObjectField(TEXT("hovered_bar"))));
		Style.SetDisabledBarImage(UCPyHome::JBrush(Payload->GetObjectField(TEXT("disabled_bar"))));
		Style.SetNormalThumbImage(UCPyHome::JBrush(Payload->GetObjectField(TEXT("normal_thumb"))));
		Style.SetHoveredThumbImage(UCPyHome::JBrush(Payload->GetObjectField(TEXT("hovered_thumb"))));
		Style.SetDisabledThumbImage(UCPyHome::JBrush(Payload->GetObjectField(TEXT("disabled_thumb"))));
		WidgetStyle = Style;
		SynchronizeProperties();
	}
	if (Method == TEXT("bar_thickness"))
	{
		WidgetStyle.SetBarThickness(In->GetNumberField(TEXT("payload")));
		SynchronizeProperties();
	}
	if (Method == TEXT("value"))
	{
		SetValue(In->GetNumberField(TEXT("payload")));
	}
	if (Method == TEXT("min_value"))
	{
		SetMinValue(In->GetNumberField(TEXT("payload")));
	}
	if (Method == TEXT("max_value"))
	{
		SetMaxValue(In->GetNumberField(TEXT("payload")));
	}
	if (Method == TEXT("orientation"))
	{
		const FString String = In->GetStringField(TEXT("payload"));
		EOrientation Enum = Orient_Horizontal;
		if (String == TEXT("Orient_Vertical")) Enum = Orient_Vertical;
		Orientation = Enum;
		SynchronizeProperties();
	}
	if (Method == TEXT("step_size"))
	{
		SetStepSize(In->GetNumberField(TEXT("payload")));
	}
	if (Method == TEXT("indent_handle"))
	{
		SetIndentHandle(In->GetBoolField(TEXT("payload")));
	}
	if (Method == TEXT("locked"))
	{
		SetLocked(In->GetBoolField(TEXT("payload")));
	}
	if (Method == TEXT("mouse_uses_step"))
	{
		MouseUsesStep = In->GetBoolField(TEXT("payload"));
		SynchronizeProperties();
	}
	return ICPyWidget::Run(Method, In, Out);
}

// ReSharper disable once CppMemberFunctionMayBeConst
void UCPySlider::OnMouseCaptureBeginEvent()
{
	UCPyHome::AddPyTask(this, TEXT("on_mouse_capture_begin"), nullptr);
}

// ReSharper disable once CppMemberFunctionMayBeConst
void UCPySlider::OnMouseCaptureEndEvent()
{
	UCPyHome::AddPyTask(this, TEXT("on_mouse_capture_end"), nullptr);
}

// ReSharper disable once CppMemberFunctionMayBeConst
void UCPySlider::OnControllerCaptureBeginEvent()
{
	UCPyHome::AddPyTask(this, TEXT("on_controller_capture_begin"), nullptr);
}

// ReSharper disable once CppMemberFunctionMayBeConst
void UCPySlider::OnControllerCaptureEndEvent()
{
	UCPyHome::AddPyTask(this, TEXT("on_controller_capture_end"), nullptr);
}

// ReSharper disable once CppMemberFunctionMayBeConst
void UCPySlider::OnValueChangedEvent(const float InValue)
{
	const TSharedPtr<FJsonObject> Payload = MakeShareable(new FJsonObject());
	Payload->SetNumberField(TEXT("value"), Value);
	UCPyHome::AddPyTask(this, TEXT("on_value_changed"), nullptr);
}

void UCPyProgressBar::Run(const FString Method, const TSharedPtr<FJsonObject> In, TSharedPtr<FJsonObject>& Out)
{
	if (Method == TEXT("style"))
	{
		const TSharedPtr<FJsonObject>& Payload = In->GetObjectField(TEXT("payload"));

		FProgressBarStyle Style;
		Style.SetFillImage(UCPyHome::JBrush(Payload->GetObjectField(TEXT("fill"))));
		Style.SetBackgroundImage(UCPyHome::JBrush(Payload->GetObjectField(TEXT("background"))));
		Style.SetMarqueeImage(UCPyHome::JBrush(Payload->GetObjectField(TEXT("marquee"))));
		WidgetStyle = Style;
		SynchronizeProperties();
	}
	if (Method == TEXT("percent"))
	{
		SetPercent(In->GetNumberField(TEXT("payload")));
	}
	if (Method == TEXT("bar_fill_type"))
	{
		const FString String = In->GetStringField(TEXT("payload"));
		EProgressBarFillType::Type Enum = EProgressBarFillType::LeftToRight;
		if (String == TEXT("RightToLeft")) Enum = EProgressBarFillType::RightToLeft;
		if (String == TEXT("FillFromCenter")) Enum = EProgressBarFillType::FillFromCenter;
		if (String == TEXT("TopToBottom")) Enum = EProgressBarFillType::TopToBottom;
		if (String == TEXT("BottomToTop")) Enum = EProgressBarFillType::BottomToTop;
		BarFillType = Enum;
		SynchronizeProperties();
	}
	if (Method == TEXT("is_marquee"))
	{
		SetIsMarquee(In->GetBoolField(TEXT("payload")));
	}
	if (Method == TEXT("border_padding"))
	{
		BorderPadding = UCPyHome::JVector2D(In->GetArrayField(TEXT("payload")));
		SynchronizeProperties();
	}
	return ICPyWidget::Run(Method, In, Out);
}

void UCPyImage::Run(const FString Method, const TSharedPtr<FJsonObject> In, TSharedPtr<FJsonObject>& Out)
{
	if (Method == TEXT("brush"))
	{
		SetBrush(UCPyHome::JBrush(In->GetObjectField(TEXT("payload"))));
	}
	return ICPyWidget::Run(Method, In, Out);
}

void UCPyTextBlock::Run(const FString Method, const TSharedPtr<FJsonObject> In, TSharedPtr<FJsonObject>& Out)
{
	if (Method == TEXT("text"))
	{
		SetText(FText::FromString(In->GetStringField(TEXT("payload"))));
	}
	if (Method == TEXT("justification"))
	{
		const FString String = In->GetStringField(TEXT("payload"));
		ETextJustify::Type Enum = ETextJustify::Left;
		if (String == TEXT("Center")) Enum = ETextJustify::Center;
		if (String == TEXT("Right")) Enum = ETextJustify::Right;
		SetJustification(Enum);
	}
	if (Method == TEXT("font"))
	{
		SetFont(UCPyHome::JFont(In->GetObjectField(TEXT("payload"))));
	}
	if (Method == TEXT("color"))
	{
		SetColorAndOpacity(UCPyHome::JLinearColor(In->GetArrayField(TEXT("payload"))));
	}
	if (Method == TEXT("min_desired_width"))
	{
		SetMinDesiredWidth(In->GetNumberField(TEXT("payload")));
	}
	if (Method == TEXT("strike_brush"))
	{
		SetStrikeBrush(UCPyHome::JBrush(In->GetObjectField(TEXT("payload"))));
	}
	if (Method == TEXT("shadow_offset"))
	{
		SetShadowOffset(UCPyHome::JVector2D(In->GetArrayField(TEXT("payload"))));
	}
	if (Method == TEXT("shadow_color"))
	{
		SetShadowColorAndOpacity(UCPyHome::JLinearColor(In->GetArrayField(TEXT("payload"))));
	}
	if (Method == TEXT("text_transform_policy"))
	{
		const FString String = In->GetStringField(TEXT("payload"));
		ETextTransformPolicy Enum = ETextTransformPolicy::None;
		if (String == TEXT("ToLower")) Enum = ETextTransformPolicy::ToLower;
		if (String == TEXT("ToUpper")) Enum = ETextTransformPolicy::ToUpper;
		SetTextTransformPolicy(Enum);
	}
	if (Method == TEXT("text_padding"))
	{
		Margin = UCPyHome::JMargin(In->GetArrayField(TEXT("payload")));
		SynchronizeProperties();
	}
	return ICPyWidget::Run(Method, In, Out);
}

void UCPySingleLineEditableTextBox::Run(const FString Method, const TSharedPtr<FJsonObject> In,
                                        TSharedPtr<FJsonObject>& Out)
{
	if (Method == TEXT("text"))
	{
		SetText(FText::FromString(In->GetStringField(TEXT("payload"))));
	}
	if (Method == TEXT("justification"))
	{
		const FString String = In->GetStringField(TEXT("payload"));
		ETextJustify::Type Enum = ETextJustify::Left;
		if (String == TEXT("Center")) Enum = ETextJustify::Center;
		if (String == TEXT("Right")) Enum = ETextJustify::Right;
		SetJustification(Enum);
	}
	if (Method == TEXT("font"))
	{
		WidgetStyle.SetFont(UCPyHome::JFont(In->GetObjectField(TEXT("payload"))));
		SynchronizeProperties();
	}
	if (Method == TEXT("is_readonly"))
	{
		SetIsReadOnly(In->GetBoolField(TEXT("payload")));
	}
	if (Method == TEXT("is_password"))
	{
		SetIsPassword(In->GetBoolField(TEXT("payload")));
	}
	if (Method == TEXT("min_desired_width"))
	{
		MinimumDesiredWidth = In->GetNumberField(TEXT("payload"));
		SynchronizeProperties();
	}
	if (Method == TEXT("foreground_color"))
	{
		WidgetStyle.SetForegroundColor(UCPyHome::JLinearColor(In->GetArrayField(TEXT("payload"))));
		SynchronizeProperties();
	}
	if (Method == TEXT("background_color"))
	{
		WidgetStyle.SetBackgroundColor(UCPyHome::JLinearColor(In->GetArrayField(TEXT("payload"))));
		SynchronizeProperties();
	}
	if (Method == TEXT("readonly_foreground_color"))
	{
		WidgetStyle.SetReadOnlyForegroundColor(UCPyHome::JLinearColor(In->GetArrayField(TEXT("payload"))));
		SynchronizeProperties();
	}
	if (Method == TEXT("text_padding"))
	{
		WidgetStyle.SetPadding(UCPyHome::JMargin(In->GetArrayField(TEXT("payload"))));
		SynchronizeProperties();
	}
	if (Method == TEXT("horizontal_scrollbar_padding"))
	{
		WidgetStyle.SetHScrollBarPadding(UCPyHome::JMargin(In->GetArrayField(TEXT("payload"))));
		SynchronizeProperties();
	}
	if (Method == TEXT("vertical_scrollbar_padding"))
	{
		WidgetStyle.SetVScrollBarPadding(UCPyHome::JMargin(In->GetArrayField(TEXT("payload"))));
		SynchronizeProperties();
	}
	if (Method == TEXT("background_style"))
	{
		const TSharedPtr<FJsonObject>& Payload = In->GetObjectField(TEXT("payload"));
		WidgetStyle.SetBackgroundImageNormal(UCPyHome::JBrush(Payload->GetObjectField(TEXT("normal"))));
		WidgetStyle.SetBackgroundImageHovered(UCPyHome::JBrush(Payload->GetObjectField(TEXT("hovered"))));
		WidgetStyle.SetBackgroundImageFocused(UCPyHome::JBrush(Payload->GetObjectField(TEXT("focused"))));
		WidgetStyle.SetBackgroundImageReadOnly(UCPyHome::JBrush(Payload->GetObjectField(TEXT("readonly"))));
		SynchronizeProperties();
	}
	if (Method == TEXT("bar_style"))
	{
		const TSharedPtr<FJsonObject>& Payload = In->GetObjectField(TEXT("payload"));

		FScrollBarStyle Style;
		Style.SetHorizontalBackgroundImage(UCPyHome::JBrush(Payload->GetObjectField(TEXT("horizontal_background"))));
		Style.SetHorizontalTopSlotImage(UCPyHome::JBrush(Payload->GetObjectField(TEXT("horizontal_top_slot"))));
		Style.SetHorizontalBottomSlotImage(UCPyHome::JBrush(Payload->GetObjectField(TEXT("horizontal_bottom_slot"))));

		Style.SetVerticalBackgroundImage(UCPyHome::JBrush(Payload->GetObjectField(TEXT("vertical_background"))));
		Style.SetVerticalTopSlotImage(UCPyHome::JBrush(Payload->GetObjectField(TEXT("vertical_top_slot"))));
		Style.SetVerticalBottomSlotImage(UCPyHome::JBrush(Payload->GetObjectField(TEXT("vertical_bottom_slot"))));

		Style.SetNormalThumbImage(UCPyHome::JBrush(Payload->GetObjectField(TEXT("normal_thumb"))));
		Style.SetHoveredThumbImage(UCPyHome::JBrush(Payload->GetObjectField(TEXT("hovered_thumb"))));
		Style.SetDraggedThumbImage(UCPyHome::JBrush(Payload->GetObjectField(TEXT("dragged_thumb"))));

		WidgetStyle.SetScrollBarStyle(Style);
		SynchronizeProperties();
	}
	if (Method == TEXT("hint_text"))
	{
		SetHintText(FText::FromString(In->GetStringField(TEXT("payload"))));
	}
	if (Method == TEXT("error_text"))
	{
		SetError(FText::FromString(In->GetStringField(TEXT("payload"))));
	}
	if (Method == TEXT("select_all_text_when_focused"))
	{
		SelectAllTextWhenFocused = In->GetBoolField(TEXT("payload"));
		SynchronizeProperties();
	}
	if (Method == TEXT("revert_text_on_escape"))
	{
		RevertTextOnEscape = In->GetBoolField(TEXT("payload"));
		SynchronizeProperties();
	}
	return ICPyWidget::Run(Method, In, Out);
}

// ReSharper disable once CppMemberFunctionMayBeConst
void UCPySingleLineEditableTextBox::OnTextChangedEvent(const FText& InText)
{
	const TSharedPtr<FJsonObject> Payload = MakeShareable(new FJsonObject());
	Payload->SetStringField(TEXT("text"), InText.ToString());
	UCPyHome::AddPyTask(this, TEXT("on_text_changed"), Payload);
}

// ReSharper disable once CppMemberFunctionMayBeConst
void UCPySingleLineEditableTextBox::OnTextCommittedEvent(const FText& InText, const ETextCommit::Type CommitMethod)
{
	FString String = TEXT("Default");
	if (CommitMethod == ETextCommit::OnEnter) String = TEXT("OnEnter");
	if (CommitMethod == ETextCommit::OnUserMovedFocus) String = TEXT("OnUserMovedFocus");
	if (CommitMethod == ETextCommit::OnCleared) String = TEXT("OnCleared");

	const TSharedPtr<FJsonObject> Payload = MakeShareable(new FJsonObject());
	Payload->SetStringField(TEXT("text"), InText.ToString());
	Payload->SetStringField(TEXT("commit_method"), String);
	UCPyHome::AddPyTask(this, TEXT("on_text_committed"), Payload);
}

void UCPyMultiLineEditableTextBox::Run(const FString Method, const TSharedPtr<FJsonObject> In,
                                       TSharedPtr<FJsonObject>& Out)
{
	if (Method == TEXT("text"))
	{
		SetText(FText::FromString(In->GetStringField(TEXT("payload"))));
	}
	if (Method == TEXT("justification"))
	{
		const FString String = In->GetStringField(TEXT("payload"));
		ETextJustify::Type Enum = ETextJustify::Left;
		if (String == TEXT("Center")) Enum = ETextJustify::Center;
		if (String == TEXT("Right")) Enum = ETextJustify::Right;
		SetJustification(Enum);
	}
	if (Method == TEXT("font"))
	{
		WidgetStyle.SetFont(UCPyHome::JFont(In->GetObjectField(TEXT("payload"))));
		SynchronizeProperties();
	}
	if (Method == TEXT("is_readonly"))
	{
		SetIsReadOnly(In->GetBoolField(TEXT("payload")));
	}
	if (Method == TEXT("foreground_color"))
	{
		WidgetStyle.SetForegroundColor(UCPyHome::JLinearColor(In->GetArrayField(TEXT("payload"))));
		SynchronizeProperties();
	}
	if (Method == TEXT("background_color"))
	{
		WidgetStyle.SetBackgroundColor(UCPyHome::JLinearColor(In->GetArrayField(TEXT("payload"))));
		SynchronizeProperties();
	}
	if (Method == TEXT("readonly_foreground_color"))
	{
		WidgetStyle.SetReadOnlyForegroundColor(UCPyHome::JLinearColor(In->GetArrayField(TEXT("payload"))));
		SynchronizeProperties();
	}
	if (Method == TEXT("text_padding"))
	{
		WidgetStyle.SetPadding(UCPyHome::JMargin(In->GetArrayField(TEXT("payload"))));
		SynchronizeProperties();
	}
	if (Method == TEXT("horizontal_scrollbar_padding"))
	{
		WidgetStyle.SetHScrollBarPadding(UCPyHome::JMargin(In->GetArrayField(TEXT("payload"))));
		SynchronizeProperties();
	}
	if (Method == TEXT("vertical_scrollbar_padding"))
	{
		WidgetStyle.SetVScrollBarPadding(UCPyHome::JMargin(In->GetArrayField(TEXT("payload"))));
		SynchronizeProperties();
	}
	if (Method == TEXT("background_style"))
	{
		const TSharedPtr<FJsonObject>& Payload = In->GetObjectField(TEXT("payload"));
		WidgetStyle.SetBackgroundImageNormal(UCPyHome::JBrush(Payload->GetObjectField(TEXT("normal"))));
		WidgetStyle.SetBackgroundImageHovered(UCPyHome::JBrush(Payload->GetObjectField(TEXT("hovered"))));
		WidgetStyle.SetBackgroundImageFocused(UCPyHome::JBrush(Payload->GetObjectField(TEXT("focused"))));
		WidgetStyle.SetBackgroundImageReadOnly(UCPyHome::JBrush(Payload->GetObjectField(TEXT("readonly"))));
		SynchronizeProperties();
	}
	if (Method == TEXT("bar_style"))
	{
		const TSharedPtr<FJsonObject>& Payload = In->GetObjectField(TEXT("payload"));

		FScrollBarStyle Style;
		Style.SetHorizontalBackgroundImage(UCPyHome::JBrush(Payload->GetObjectField(TEXT("horizontal_background"))));
		Style.SetHorizontalTopSlotImage(UCPyHome::JBrush(Payload->GetObjectField(TEXT("horizontal_top_slot"))));
		Style.SetHorizontalBottomSlotImage(UCPyHome::JBrush(Payload->GetObjectField(TEXT("horizontal_bottom_slot"))));

		Style.SetVerticalBackgroundImage(UCPyHome::JBrush(Payload->GetObjectField(TEXT("vertical_background"))));
		Style.SetVerticalTopSlotImage(UCPyHome::JBrush(Payload->GetObjectField(TEXT("vertical_top_slot"))));
		Style.SetVerticalBottomSlotImage(UCPyHome::JBrush(Payload->GetObjectField(TEXT("vertical_bottom_slot"))));

		Style.SetNormalThumbImage(UCPyHome::JBrush(Payload->GetObjectField(TEXT("normal_thumb"))));
		Style.SetHoveredThumbImage(UCPyHome::JBrush(Payload->GetObjectField(TEXT("hovered_thumb"))));
		Style.SetDraggedThumbImage(UCPyHome::JBrush(Payload->GetObjectField(TEXT("dragged_thumb"))));

		WidgetStyle.SetScrollBarStyle(Style);
		SynchronizeProperties();
	}
	if (Method == TEXT("hint_text"))
	{
		SetHintText(FText::FromString(In->GetStringField(TEXT("payload"))));
	}
	if (Method == TEXT("error_text"))
	{
		SetError(FText::FromString(In->GetStringField(TEXT("payload"))));
	}
	return ICPyWidget::Run(Method, In, Out);
}

// ReSharper disable once CppMemberFunctionMayBeConst
void UCPyMultiLineEditableTextBox::OnTextChangedEvent(const FText& InText)
{
	const TSharedPtr<FJsonObject> Payload = MakeShareable(new FJsonObject());
	Payload->SetStringField(TEXT("text"), InText.ToString());
	UCPyHome::AddPyTask(this, TEXT("on_text_changed"), Payload);
}

// ReSharper disable once CppMemberFunctionMayBeConst
void UCPyMultiLineEditableTextBox::OnTextCommittedEvent(const FText& InText, const ETextCommit::Type CommitMethod)
{
	FString String = TEXT("Default");
	if (CommitMethod == ETextCommit::OnEnter) String = TEXT("OnEnter");
	if (CommitMethod == ETextCommit::OnUserMovedFocus) String = TEXT("OnUserMovedFocus");
	if (CommitMethod == ETextCommit::OnCleared) String = TEXT("OnCleared");

	const TSharedPtr<FJsonObject> Payload = MakeShareable(new FJsonObject());
	Payload->SetStringField(TEXT("text"), InText.ToString());
	Payload->SetStringField(TEXT("commit_method"), String);
	UCPyHome::AddPyTask(this, TEXT("on_text_committed"), Payload);
}

void UCPySpinBox::Run(const FString Method, const TSharedPtr<FJsonObject> In, TSharedPtr<FJsonObject>& Out)
{
	if (Method == TEXT("value"))
	{
		SetValue(In->GetNumberField(TEXT("payload")));
	}
	if (Method == TEXT("min_value"))
	{
		SetMinValue(In->GetNumberField(TEXT("payload")));
	}
	if (Method == TEXT("max_value"))
	{
		SetMaxValue(In->GetNumberField(TEXT("payload")));
	}
	if (Method == TEXT("min_slider_value"))
	{
		SetMinSliderValue(In->GetNumberField(TEXT("payload")));
	}
	if (Method == TEXT("max_slider_value"))
	{
		SetMaxSliderValue(In->GetNumberField(TEXT("payload")));
	}
	if (Method == TEXT("slider_exponent"))
	{
		SliderExponent = In->GetNumberField(TEXT("payload"));
		SynchronizeProperties();
	}
	if (Method == TEXT("min_fractional_digits"))
	{
		SetMinFractionalDigits(In->GetIntegerField(TEXT("payload")));
	}
	if (Method == TEXT("max_fractional_digits"))
	{
		SetMaxFractionalDigits(In->GetIntegerField(TEXT("payload")));
	}
	if (Method == TEXT("delta"))
	{
		SetDelta(In->GetNumberField(TEXT("payload")));
	}
	if (Method == TEXT("always_uses_delta_snap"))
	{
		SetAlwaysUsesDeltaSnap(In->GetBoolField(TEXT("payload")));
	}
	if (Method == TEXT("font"))
	{
		Font = UCPyHome::JFont(In->GetObjectField(TEXT("payload")));
		SynchronizeProperties();
	}
	if (Method == TEXT("foreground_color"))
	{
		SetForegroundColor(UCPyHome::JLinearColor(In->GetArrayField(TEXT("payload"))));
	}
	if (Method == TEXT("justification"))
	{
		const FString String = In->GetStringField(TEXT("payload"));
		ETextJustify::Type Enum = ETextJustify::Left;
		if (String == TEXT("Center")) Enum = ETextJustify::Center;
		if (String == TEXT("Right")) Enum = ETextJustify::Right;
		Justification = Enum;
		SynchronizeProperties();
	}
	if (Method == TEXT("min_desired_width"))
	{
		MinDesiredWidth = In->GetNumberField(TEXT("payload"));
		SynchronizeProperties();
	}
	if (Method == TEXT("style"))
	{
		const TSharedPtr<FJsonObject>& Payload = In->GetObjectField(TEXT("payload"));

		WidgetStyle.SetBackgroundBrush(UCPyHome::JBrush(Payload->GetObjectField(TEXT("background"))));
		WidgetStyle.SetHoveredBackgroundBrush(UCPyHome::JBrush(Payload->GetObjectField(TEXT("hovered_background"))));
		WidgetStyle.SetActiveFillBrush(UCPyHome::JBrush(Payload->GetObjectField(TEXT("active_fill"))));
		WidgetStyle.SetInactiveFillBrush(UCPyHome::JBrush(Payload->GetObjectField(TEXT("inactive_fill"))));
		WidgetStyle.SetArrowsImage(UCPyHome::JBrush(Payload->GetObjectField(TEXT("arrows"))));
	}
	if (Method == TEXT("text_padding"))
	{
		WidgetStyle.SetTextPadding(UCPyHome::JMargin(In->GetArrayField(TEXT("payload"))));
	}
	return ICPyWidget::Run(Method, In, Out);
}

// ReSharper disable once CppMemberFunctionMayBeConst
void UCPySpinBox::OnValueChangedEvent(const float InValue)
{
	const TSharedPtr<FJsonObject> Payload = MakeShareable(new FJsonObject());
	Payload->SetNumberField(TEXT("value"), InValue);
	Payload->SetNumberField(TEXT("min_value"), MinValue);
	Payload->SetNumberField(TEXT("max_value"), MaxValue);
	UCPyHome::AddPyTask(this, TEXT("on_value_changed"), Payload);
}

// ReSharper disable once CppMemberFunctionMayBeConst
void UCPySpinBox::OnValueCommittedEvent(const float InValue, const ETextCommit::Type CommitMethod)
{
	FString String = TEXT("Default");
	if (CommitMethod == ETextCommit::OnEnter) String = TEXT("OnEnter");
	if (CommitMethod == ETextCommit::OnUserMovedFocus) String = TEXT("OnUserMovedFocus");
	if (CommitMethod == ETextCommit::OnCleared) String = TEXT("OnCleared");

	const TSharedPtr<FJsonObject> Payload = MakeShareable(new FJsonObject());
	Payload->SetNumberField(TEXT("value"), InValue);
	Payload->SetNumberField(TEXT("min_value"), MinValue);
	Payload->SetNumberField(TEXT("max_value"), MaxValue);
	Payload->SetStringField(TEXT("commit_method"), String);
	UCPyHome::AddPyTask(this, TEXT("on_value_committed"), Payload);
}

// ReSharper disable once CppMemberFunctionMayBeConst
void UCPySpinBox::OnBeginSliderMovementEvent()
{
	UCPyHome::AddPyTask(this, TEXT("on_begin_slider_movement"), nullptr);
}

// ReSharper disable once CppMemberFunctionMayBeConst
void UCPySpinBox::OnEndSliderMovementEvent(const float InValue)
{
	const TSharedPtr<FJsonObject> Payload = MakeShareable(new FJsonObject());
	Payload->SetNumberField(TEXT("value"), InValue);
	UCPyHome::AddPyTask(this, TEXT("on_end_slider_movement"), Payload);
}

void UCPySpacer::Run(const FString Method, const TSharedPtr<FJsonObject> In, TSharedPtr<FJsonObject>& Out)
{
	if (Method == TEXT("size"))
	{
		SetSize(UCPyHome::JVector2D(In->GetArrayField(TEXT("payload"))));
	}
	return ICPyWidget::Run(Method, In, Out);
}
