// Fill out your copyright notice in the Description page of Project Settings.


#include "CPlayerController.h"
#include "Kismet/GameplayStatics.h"

#define LOCTEXT_NAMESPACE "CPlayerController"

FString ACPlayerController::StorageRoot;
FString ACPlayerController::DefaultStorageRoot;

DEFINE_LOG_CATEGORY(LogCPlayerController);

void ACPlayerController::Log(const FString Message)
{
	UE_LOG(LogCPlayerController, Display, TEXT("%ls"), *Message);
	GEngine->AddOnScreenDebugMessage(INDEX_NONE, 10, FColor::Yellow, Message);
}

void ACPlayerController::LogTask(const FString Key, const FString Step, const float Progress, const FString Error)
{
	FString ProgressMark;
	for (uint32 i = 0; i < 10; ++i)
	{
		ProgressMark += i < 10 * Progress ? TEXT("#") : TEXT("-");
	}

	FString Message = FString::Printf(TEXT("%ls %ls %ls"), *ProgressMark, *Key, *Step);
	if (!Error.IsEmpty())
	{
		Message += TEXT(" ") + Error;
	}
	Log(Message);
}

ACPlayerController::ACPlayerController()
{
	bShowMouseCursor = true;

	static bool HasStorageRoot = false;
	if (!HasStorageRoot)
	{
		const FString DownloadDir = FPaths::ConvertRelativePathToFull(FPaths::ProjectPersistentDownloadDir());
		DefaultStorageRoot = FPaths::Combine(DownloadDir, TEXT("Storage"));
		if (!GConfig->GetString(TEXT("LanStorage"), TEXT("Root"), StorageRoot, GGameUserSettingsIni))
		{
			StorageRoot = DefaultStorageRoot;
		}
	}
}

void ACPlayerController::BeginPlay()
{
	Super::BeginPlay();
	InitRemoteRole();
}

void ACPlayerController::Tick(const float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	uint32 i = 0;
	while (TaskDataTransfer.Num() > 0 && i < 10)
	{
		for (const auto Item : TaskDataTransfer)
		{
			if (TaskDataTransfer[Item.Key] < TaskData[Item.Key].Num())
			{
				TaskDataAdd(Item.Key, TaskData[Item.Key][TaskDataTransfer[Item.Key]]);
				++TaskDataTransfer[Item.Key];
			}
			else
			{
				TaskDataTransfer.Remove(Item.Key);
				if (TaskDataTransfer.Num() == 0)
				{
					InitTransfer(0);
					OnTransferProgress.Broadcast(0, 0);
				}
			}
			break;
		}
		++i;
	}

	if (TransferSize > 0)
	{
		const double Send = SendBytes / static_cast<double>(TransferSize);
		const double Receive = ReceiveBytes / static_cast<double>(TransferSize);
		OnTransferProgress.Broadcast(Send, Receive);
	}
	else
	{
		if (TaskKey == TEXT("UploadFile"))
		{
			UploadFile();
		}
	}

	if ((FDateTime::UtcNow() - OneSecTimer).GetTotalSeconds() > 1)
	{
		OneSecTimer = FDateTime::UtcNow();

		FNumberFormattingOptions Options;
		Options.UseGrouping = false;
		Options.RoundingMode = HalfFromZero;
		Options.MinimumIntegralDigits = 1;
		Options.MaximumIntegralDigits = 9;
		Options.MinimumFractionalDigits = 1;
		Options.MaximumFractionalDigits = 1;

		const FString Send = FText::AsMemory(SendBytesPerSecond, &Options).ToString();
		const FString Receive = FText::AsMemory(ReceiveBytesPerSecond, &Options).ToString();
		OnTransferSpeed.Broadcast(Send, Receive);

		SendBytesPerSecond = 0, ReceiveBytesPerSecond = 0;
	}
}

template <typename T>
TArray<TArray<uint8>> ACPlayerController::ToChunks(T Content)
{
	TArray<uint8> Bytes;
	FMemoryWriter W(Bytes);
	W << Content;

	const uint32 PartNum = Bytes.Num() / ChunkSize + (Bytes.Num() % ChunkSize ? 1 : 0);

	TArray<TArray<uint8>> Chunks;

	for (uint32 Part = 0; Part < PartNum; ++Part)
	{
		const uint32 Count = FMath::Min(ChunkSize, Bytes.Num() - Part * ChunkSize);
		Chunks.Add(TArray<uint8>(Bytes.GetData() + Part * ChunkSize, Count));
	}

	return Chunks;
}

template <typename T>
T ACPlayerController::FromChunks(const TArray<TArray<uint8>>& Chunks)
{
	TArray<uint8> Bytes;

	for (const TArray<uint8>& Chunk : Chunks)
	{
		Bytes.Append(Chunk);
	}

	T Content;
	FMemoryReader R(Bytes);
	R << Content;
	return Content;
}

void ACPlayerController::InitRemoteRole()
{
	bRemoteClient = false;
	bRemoteServer = false;
	RemoteRoleGuid = FGuid::NewGuid();
	InitRemoteRole_Server(RemoteRoleGuid);
}

void ACPlayerController::InitRemoteRole_Server_Implementation(const FGuid Guid)
{
	if (RemoteRoleGuid != Guid)
	{
		SetRemoteClient();
	}
}

void ACPlayerController::SetRemoteClient_Implementation()
{
	bRemoteClient = true;
	bRemoteServer = false;
	SetRemoteServer();
}

void ACPlayerController::SetRemoteServer_Implementation()
{
	bRemoteServer = true;
	bRemoteClient = false;
}

void ACPlayerController::TaskDataEmpty()
{
	TaskData.Empty();
	if (bRemoteServer) { TaskDataEmpty_Client(); }
	if (bRemoteClient) { TaskDataEmpty_Server(); }
}

void ACPlayerController::TaskDataEmpty_Client_Implementation()
{
	TaskData.Empty();
}

void ACPlayerController::TaskDataEmpty_Server_Implementation()
{
	TaskData.Empty();
}

void ACPlayerController::TaskDataAdd(const FString& Key, const TArray<uint8>& Chunk)
{
	if (bRemoteServer) { TaskDataAdd_Client(Key, Chunk); }
	if (bRemoteClient) { TaskDataAdd_Server(Key, Chunk); }
}

void ACPlayerController::TaskDataAdd_Server_Implementation(const FString& Key, const TArray<uint8>& Chunk)
{
	if (TaskData.Contains(Key)) { TaskData[Key].Add(Chunk); }
	else { TaskData.Add(Key, {Chunk}); }
	AddSendBytes(Chunk.Num());
}

void ACPlayerController::TaskDataAdd_Client_Implementation(const FString& Key, const TArray<uint8>& Chunk)
{
	if (TaskData.Contains(Key)) { TaskData[Key].Add(Chunk); }
	else { TaskData.Add(Key, {Chunk}); }
	AddReceiveBytes(Chunk.Num());
}

void ACPlayerController::InitTransfer(const uint32 Size)
{
	TransferSize = Size;
	SendBytes = 0;
	ReceiveBytes = 0;
	if (bRemoteServer) { InitTransfer_Client(TransferSize); }
	if (bRemoteClient) { InitTransfer_Server(TransferSize); }
}

void ACPlayerController::InitTransfer_Server_Implementation(const uint32 Size)
{
	TransferSize = Size;
	SendBytes = 0;
	ReceiveBytes = 0;
}

void ACPlayerController::InitTransfer_Client_Implementation(const uint32 Size)
{
	TransferSize = Size;
	SendBytes = 0;
	ReceiveBytes = 0;
}

void ACPlayerController::AddSendBytes(const uint32 Num)
{
	SendBytesPerSecond += Num;
	SendBytes += Num;
	if (bRemoteServer) { AddSendBytes_Client(Num); }
	if (bRemoteClient) { AddSendBytes_Server(Num); }
}

void ACPlayerController::AddSendBytes_Client_Implementation(const uint32 Num)
{
	SendBytesPerSecond += Num;
	SendBytes += Num;
}

void ACPlayerController::AddSendBytes_Server_Implementation(const uint32 Num)
{
	SendBytesPerSecond += Num;
	SendBytes += Num;
}

void ACPlayerController::AddReceiveBytes(const uint32 Num)
{
	ReceiveBytesPerSecond += Num;
	ReceiveBytes += Num;
	if (bRemoteServer) { AddReceiveBytes_Client(Num); }
	if (bRemoteClient) { AddReceiveBytes_Server(Num); }
}

void ACPlayerController::AddReceiveBytes_Server_Implementation(const uint32 Num)
{
	ReceiveBytesPerSecond += Num;
	ReceiveBytes += Num;
}

void ACPlayerController::AddReceiveBytes_Client_Implementation(const uint32 Num)
{
	ReceiveBytesPerSecond += Num;
	ReceiveBytes += Num;
}

void ACPlayerController::SetTask(const FText& Name, const FString& Key, const FString& Step, const float Progress,
                                 const FString& Error)
{
	TaskName = Name;
	TaskKey = Key;
	TaskStep = Step;
	TaskProgress = Progress;
	TaskError = Error;
	OnTask.Broadcast(TaskName, TaskStep, TaskProgress, TaskError);
	if (bRemoteServer) { SetTask_Client(Name, Key, Step, Progress, Error); }
	if (bRemoteClient) { SetTask_Server(Name, Key, Step, Progress, Error); }
}

void ACPlayerController::SetTask_Server_Implementation(const FText& Name, const FString& Key, const FString& Step, const float Progress,
                                                       const FString& Error)
{
	TaskName = Name;
	TaskKey = Key;
	TaskStep = Step;
	TaskProgress = Progress;
	TaskError = Error;
	OnTask.Broadcast(TaskName, TaskStep, TaskProgress, TaskError);
}

void ACPlayerController::SetTask_Client_Implementation(const FText& Name, const FString& Key, const FString& Step, const float Progress,
                                                       const FString& Error)
{
	TaskName = Name;
	TaskKey = Key;
	TaskStep = Step;
	TaskProgress = Progress;
	TaskError = Error;
	OnTask.Broadcast(TaskName, TaskStep, TaskProgress, TaskError);
}

void ACPlayerController::UploadFile(const FString& Filename, const FString& SavePath)
{
	if (!TaskKey.IsEmpty()) { return; }

	TaskDataEmpty();
	TaskData.Add(TEXT("Filename"), ToChunks(Filename));
	TaskData.Add(TEXT("SavePath"), ToChunks(SavePath));
	return SetTask(LOCTEXT("UploadFile", "UploadFile"), TEXT("UploadFile"), TEXT("Upload"), 0.1);
}

void ACPlayerController::UploadFile()
{
	if (!bRemoteServer && TaskStep == TEXT("Upload"))
	{
		const FString Filename = FromChunks<FString>(TaskData[TEXT("Filename")]);

		TArray<uint8> FileBytes;
		if (!FFileHelper::LoadFileToArray(FileBytes, *Filename))
		{
			const FString Error = FString::Printf(TEXT("Failed to read file %ls"), *Filename);
			LogTask(TaskKey, TaskStep, TaskProgress, Error);
			return SetTask(TaskName, TaskKey, TEXT("End"), TaskProgress, Error);
		}

		TaskData.Add(TEXT("FileBytes"), ToChunks(FileBytes));

		if (bRemoteClient)
		{
			TaskDataTransfer.Add(TEXT("Filename"), 0);
			TaskDataTransfer.Add(TEXT("SavePath"), 0);
			TaskDataTransfer.Add(TEXT("FileBytes"), 0);

			uint32 Total = 0;
			for (const auto Item : TaskDataTransfer)
			{
				Total += FMath::Max(TaskData[Item.Key].Num() - 1, 0) * ChunkSize + TaskData[Item.Key].Last().Num();
			}
			InitTransfer(Total);
		}

		LogTask(TaskKey, TaskStep, TaskProgress);
		return SetTask(TaskName, TaskKey, TEXT("Save"), 0.2);
	}
	if (!bRemoteClient && TaskStep == TEXT("Save"))
	{
		FString Filename = FromChunks<FString>(TaskData[TEXT("Filename")]);
		Filename = FPaths::GetCleanFilename(Filename);

		FString SavePath = FromChunks<FString>(TaskData[TEXT("SavePath")]);
		SavePath = FPaths::Combine(StorageRoot, SavePath, Filename);

		FText Reason;
		if (!FPaths::ValidatePath(SavePath, &Reason))
		{
			FString Error = TEXT("Invalid storage or category ");
			Error += FString::Printf(TEXT("%ls %ls"), *Reason.ToString(), *SavePath);
			LogTask(TaskKey, TaskStep, TaskProgress, Error);
			return SetTask(TaskName, TaskKey, TEXT("End"), TaskProgress, Error);
		}

		const TArray<uint8> FileBytes = FromChunks<TArray<uint8>>(TaskData[TEXT("FileBytes")]);

		if (!FFileHelper::SaveArrayToFile(FileBytes, *SavePath))
		{
			const FString Error = FString::Printf(TEXT("Failed to write file %ls"), *SavePath);
			LogTask(TaskKey, TaskStep, TaskProgress, Error);
			return SetTask(TaskName, TaskKey, TEXT("End"), TaskProgress, Error);
		}

		LogTask(TaskKey, TaskStep, TaskProgress);
		return SetTask(TaskName, TaskKey, TEXT("End"), 1.0);
	}
	if (!bRemoteServer && TaskStep == TEXT("End"))
	{
		TaskDataEmpty();
		LogTask(TaskKey, TaskStep, TaskProgress);
		return SetTask({}, {}, {}, 0);
	}
}

#undef LOCTEXT_NAMESPACE
