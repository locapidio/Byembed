// Fill out your copyright notice in the Description page of Project Settings.


#include "CPyInterface.h"

#include "CPyHome.h"
#include "Components/ButtonSlot.h"
#include "Components/BackgroundBlurSlot.h"
#include "Components/BorderSlot.h"
#include "Components/CanvasPanelSlot.h"
#include "Components/GridSlot.h"
#include "Components/HorizontalBoxSlot.h"
#include "Components/OverlaySlot.h"
#include "Components/ScaleBoxSlot.h"
#include "Components/ScrollBoxSlot.h"
#include "Components/SizeBoxSlot.h"
#include "Components/UniformGridSlot.h"
#include "Components/VerticalBoxSlot.h"
#include "Components/WidgetSwitcherSlot.h"
#include "Components/WrapBoxSlot.h"
#include "Components/Widget.h"

// Add default functionality here for any ICPyInterface functions that are not pure virtual.

void ICPyWidget::Run(const FString Method, const TSharedPtr<FJsonObject> In, TSharedPtr<FJsonObject>& Out)
{
	UWidget* This = Cast<UWidget>(this);
	if (!IsValid(This)) return;

	if (Method == TEXT("delete"))
	{
		This->RemoveFromParent();
		UCPyHome::Get()->Delete(This);
	}
	if (Method == TEXT("tooltip_text"))
	{
		This->SetToolTipText(FText::FromString(In->GetStringField(TEXT("payload"))));
	}
	if (Method == TEXT("is_enabled"))
	{
		This->SetIsEnabled(In->GetBoolField(TEXT("payload")));
	}
	if (Method == TEXT("visibility"))
	{
		const FString String = In->GetStringField(TEXT("payload"));
		ESlateVisibility Enum = ESlateVisibility::Visible;
		if (String == TEXT("Collapsed")) Enum = ESlateVisibility::Collapsed;
		if (String == TEXT("Hidden")) Enum = ESlateVisibility::Hidden;
		if (String == TEXT("HitTestInvisible")) Enum = ESlateVisibility::HitTestInvisible;
		if (String == TEXT("SelfHitTestInvisible")) Enum = ESlateVisibility::SelfHitTestInvisible;
		This->SetVisibility(Enum);
	}
	if (Method == TEXT("cursor"))
	{
		const FString String = In->GetStringField(TEXT("payload"));
		EMouseCursor::Type Enum = EMouseCursor::Default;
		if (String == TEXT("None")) Enum = EMouseCursor::None;
		if (String == TEXT("TextEditBeam")) Enum = EMouseCursor::TextEditBeam;
		if (String == TEXT("ResizeLeftRight")) Enum = EMouseCursor::ResizeLeftRight;
		if (String == TEXT("ResizeUpDown")) Enum = EMouseCursor::ResizeUpDown;
		if (String == TEXT("ResizeSouthEast")) Enum = EMouseCursor::ResizeSouthEast;
		if (String == TEXT("ResizeSouthWest")) Enum = EMouseCursor::ResizeSouthWest;
		if (String == TEXT("CardinalCross")) Enum = EMouseCursor::CardinalCross;
		if (String == TEXT("Crosshairs")) Enum = EMouseCursor::Crosshairs;
		if (String == TEXT("Hand")) Enum = EMouseCursor::Hand;
		if (String == TEXT("GrabHand")) Enum = EMouseCursor::GrabHand;
		if (String == TEXT("GrabHandClosed")) Enum = EMouseCursor::GrabHandClosed;
		if (String == TEXT("SlashedCircle")) Enum = EMouseCursor::SlashedCircle;
		if (String == TEXT("EyeDropper")) Enum = EMouseCursor::EyeDropper;
		if (String == TEXT("Custom")) Enum = EMouseCursor::Custom;
		if (String == TEXT("TotalCursorCount")) Enum = EMouseCursor::TotalCursorCount;
		This->SetCursor(Enum);
	}
	if (Method == TEXT("render_opacity"))
	{
		This->SetRenderOpacity(In->GetNumberField(TEXT("payload")));
	}
	if (Method == TEXT("render_translation"))
	{
		This->SetRenderTranslation(UCPyHome::JVector2D(In->GetArrayField(TEXT("payload"))));
	}
	if (Method == TEXT("render_scale"))
	{
		This->SetRenderScale(UCPyHome::JVector2D(In->GetArrayField(TEXT("payload"))));
	}
	if (Method == TEXT("render_shear"))
	{
		This->SetRenderShear(UCPyHome::JVector2D(In->GetArrayField(TEXT("payload"))));
	}
	if (Method == TEXT("render_transform_angle"))
	{
		This->SetRenderTransformAngle(In->GetNumberField(TEXT("payload")));
	}
	if (Method == TEXT("render_transform_pivot"))
	{
		This->SetRenderTransformPivot(UCPyHome::JVector2D(In->GetArrayField(TEXT("payload"))));
	}
	if (Method == TEXT("add_child"))
	{
		UPanelWidget* PanelWidget = Cast<UPanelWidget>(This);
		const FString Id = In->GetStringField(TEXT("payload"));
		if (PanelWidget && UCPyHome::Get()->PyInstances.Contains(Id))
		{
			if (UWidget* Widget = Cast<UWidget>(UCPyHome::Get()->PyInstances[Id]))
			{
				PanelWidget->AddChild(Widget);
			}
		}
	}
	if (Method == TEXT("remove_child"))
	{
		UPanelWidget* PanelWidget = Cast<UPanelWidget>(This);
		const FString Id = In->GetStringField(TEXT("payload"));
		if (PanelWidget && UCPyHome::Get()->PyInstances.Contains(Id))
		{
			if (UWidget* Widget = Cast<UWidget>(UCPyHome::Get()->PyInstances[Id]))
			{
				PanelWidget->RemoveChild(Widget);
			}
		}
	}
	if (Method == TEXT("clear_children"))
	{
		if (UPanelWidget* PanelWidget = Cast<UPanelWidget>(This))
		{
			PanelWidget->ClearChildren();
		}
	}
	if (Method == TEXT("anchors"))
	{
		const TArray<TSharedPtr<FJsonValue>>& Payload = In->GetArrayField(TEXT("payload"));

		FAnchors Anchors;
		Anchors.Minimum.X = Payload[0]->AsArray()[0]->AsNumber();
		Anchors.Minimum.Y = Payload[0]->AsArray()[1]->AsNumber();
		Anchors.Maximum.X = Payload[1]->AsArray()[0]->AsNumber();
		Anchors.Maximum.Y = Payload[1]->AsArray()[1]->AsNumber();

		if (const auto S = Cast<UCanvasPanelSlot>(This->Slot))
		{
			S->SetAnchors(Anchors);
		}
	}
	if (Method == TEXT("pivot"))
	{
		if (const auto S = Cast<UCanvasPanelSlot>(This->Slot))
		{
			S->SetAlignment(UCPyHome::JVector2D(In->GetArrayField(TEXT("payload"))));
		}
	}
	if (Method == TEXT("auto_size"))
	{
		const bool bEnabled = In->GetBoolField(TEXT("payload"));
		if (const auto S = Cast<UCanvasPanelSlot>(This->Slot))
		{
			S->SetAutoSize(bEnabled);
		}
	}
	if (Method == TEXT("z_order"))
	{
		const int32 Value = In->GetIntegerField(TEXT("payload"));
		if (const auto S = Cast<UCanvasPanelSlot>(This->Slot))
		{
			S->SetZOrder(Value);
		}
	}
	if (Method == TEXT("offsets"))
	{
		FMargin Value;
		Value.Left = In->GetArrayField(TEXT("payload"))[0]->AsNumber();
		Value.Right = In->GetArrayField(TEXT("payload"))[1]->AsNumber();
		Value.Top = In->GetArrayField(TEXT("payload"))[2]->AsNumber();
		Value.Bottom = In->GetArrayField(TEXT("payload"))[3]->AsNumber();

		if (const auto S = Cast<UCanvasPanelSlot>(This->Slot))
		{
			S->SetOffsets(Value);
		}
	}
	if (Method == TEXT("padding"))
	{
		FMargin Value;
		Value.Left = In->GetArrayField(TEXT("payload"))[0]->AsNumber();
		Value.Right = In->GetArrayField(TEXT("payload"))[1]->AsNumber();
		Value.Top = In->GetArrayField(TEXT("payload"))[2]->AsNumber();
		Value.Bottom = In->GetArrayField(TEXT("payload"))[3]->AsNumber();

		if (const auto S = Cast<UBackgroundBlurSlot>(This->Slot))
		{
			S->SetPadding(Value);
		}
		if (const auto S = Cast<UBorderSlot>(This->Slot))
		{
			S->SetPadding(Value);
		}
		if (const auto S = Cast<UGridSlot>(This->Slot))
		{
			S->SetPadding(Value);
		}
		if (const auto S = Cast<UHorizontalBoxSlot>(This->Slot))
		{
			S->SetPadding(Value);
		}
		if (const auto S = Cast<UVerticalBoxSlot>(This->Slot))
		{
			S->SetPadding(Value);
		}
		if (const auto S = Cast<UOverlaySlot>(This->Slot))
		{
			S->SetPadding(Value);
		}
		if (const auto S = Cast<UScaleBoxSlot>(This->Slot))
		{
			S->SetPadding(Value);
		}
		if (const auto S = Cast<UScrollBoxSlot>(This->Slot))
		{
			S->SetPadding(Value);
		}
		if (const auto S = Cast<USizeBoxSlot>(This->Slot))
		{
			S->SetPadding(Value);
		}
		if (const auto S = Cast<UWidgetSwitcherSlot>(This->Slot))
		{
			S->SetPadding(Value);
		}
		if (const auto S = Cast<UWrapBoxSlot>(This->Slot))
		{
			S->SetPadding(Value);
		}
		if (const auto S = Cast<UButtonSlot>(This->Slot))
		{
			S->SetPadding(Value);
		}
	}
	if (Method == TEXT("alignment"))
	{
		const FString H = In->GetArrayField(TEXT("payload"))[0]->AsString();
		EHorizontalAlignment HAlign = HAlign_Fill;
		if (H == TEXT("Left")) HAlign = HAlign_Left;
		if (H == TEXT("Center")) HAlign = HAlign_Center;
		if (H == TEXT("Right")) HAlign = HAlign_Right;

		const FString V = In->GetArrayField(TEXT("payload"))[1]->AsString();
		EVerticalAlignment VAlign = VAlign_Fill;
		if (V == TEXT("Top")) VAlign = VAlign_Top;
		if (V == TEXT("Center")) VAlign = VAlign_Center;
		if (V == TEXT("Bottom")) VAlign = VAlign_Bottom;

		if (const auto S = Cast<UBackgroundBlurSlot>(This->Slot))
		{
			S->SetHorizontalAlignment(HAlign);
			S->SetVerticalAlignment(VAlign);
		}
		if (const auto S = Cast<UBorderSlot>(This->Slot))
		{
			S->SetHorizontalAlignment(HAlign);
			S->SetVerticalAlignment(VAlign);
		}
		if (const auto S = Cast<UGridSlot>(This->Slot))
		{
			S->SetHorizontalAlignment(HAlign);
			S->SetVerticalAlignment(VAlign);
		}
		if (const auto S = Cast<UHorizontalBoxSlot>(This->Slot))
		{
			S->SetHorizontalAlignment(HAlign);
			S->SetVerticalAlignment(VAlign);
		}
		if (const auto S = Cast<UVerticalBoxSlot>(This->Slot))
		{
			S->SetHorizontalAlignment(HAlign);
			S->SetVerticalAlignment(VAlign);
		}
		if (const auto S = Cast<UOverlaySlot>(This->Slot))
		{
			S->SetHorizontalAlignment(HAlign);
			S->SetVerticalAlignment(VAlign);
		}
		if (const auto S = Cast<UScaleBoxSlot>(This->Slot))
		{
			S->SetHorizontalAlignment(HAlign);
			S->SetVerticalAlignment(VAlign);
		}
		if (const auto S = Cast<UScrollBoxSlot>(This->Slot))
		{
			S->SetHorizontalAlignment(HAlign);
			S->SetVerticalAlignment(VAlign);
		}
		if (const auto S = Cast<USizeBoxSlot>(This->Slot))
		{
			S->SetHorizontalAlignment(HAlign);
			S->SetVerticalAlignment(VAlign);
		}
		if (const auto S = Cast<UUniformGridSlot>(This->Slot))
		{
			S->SetHorizontalAlignment(HAlign);
			S->SetVerticalAlignment(VAlign);
		}
		if (const auto S = Cast<UWidgetSwitcherSlot>(This->Slot))
		{
			S->SetHorizontalAlignment(HAlign);
			S->SetVerticalAlignment(VAlign);
		}
		if (const auto S = Cast<UWrapBoxSlot>(This->Slot))
		{
			S->SetHorizontalAlignment(HAlign);
			S->SetVerticalAlignment(VAlign);
		}
		if (const auto S = Cast<UButtonSlot>(This->Slot))
		{
			S->SetHorizontalAlignment(HAlign);
			S->SetVerticalAlignment(VAlign);
		}
	}
	if (Method == TEXT("grid_index"))
	{
		const int32 Row = In->GetArrayField(TEXT("payload"))[0]->AsNumber();
		const int32 Column = In->GetArrayField(TEXT("payload"))[1]->AsNumber();

		if (const auto S = Cast<UGridSlot>(This->Slot))
		{
			S->SetRow(Row);
			S->SetColumn(Column);
		}
		if (const auto S = Cast<UUniformGridSlot>(This->Slot))
		{
			S->SetRow(Row);
			S->SetColumn(Column);
		}
	}
	if (Method == TEXT("grid_span"))
	{
		const int32 Row = In->GetArrayField(TEXT("payload"))[0]->AsNumber();
		const int32 Column = In->GetArrayField(TEXT("payload"))[1]->AsNumber();

		if (const auto S = Cast<UGridSlot>(This->Slot))
		{
			S->SetRowSpan(Row);
			S->SetColumnSpan(Column);
		}
	}
	if (Method == TEXT("size_rule"))
	{
		const FString Rule = In->GetArrayField(TEXT("payload"))[0]->AsString();
		FSlateChildSize Size;
		Size.SizeRule = ESlateSizeRule::Automatic;
		if (Rule == TEXT("fill")) Size.SizeRule = ESlateSizeRule::Fill;
		Size.Value = In->GetArrayField(TEXT("payload"))[1]->AsNumber();

		if (const auto S = Cast<UHorizontalBoxSlot>(This->Slot))
		{
			S->SetSize(Size);
		}
		if (const auto S = Cast<UVerticalBoxSlot>(This->Slot))
		{
			S->SetSize(Size);
		}
	}
	if (Method == TEXT("fill_empty_space"))
	{
		if (const auto S = Cast<UWrapBoxSlot>(This->Slot))
		{
			S->SetFillEmptySpace(In->GetBoolField(TEXT("payload")));
		}
	}
	if (Method == TEXT("fill_span_when_less_than"))
	{
		if (const auto S = Cast<UWrapBoxSlot>(This->Slot))
		{
			S->SetFillSpanWhenLessThan(In->GetNumberField(TEXT("payload")));
		}
	}
}
