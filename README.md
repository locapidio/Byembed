# Byembed

A plugin for Unreal Python at runtime intergrated with [locapip](https://gitlab.com/locapidio/locapip)
and [locapic](https://gitlab.com/locapidio/locapic)

虚幻引擎Python运行时插件，结合 [locapip](https://gitlab.com/locapidio/locapip) 和 [locapic](https://gitlab.com/locapidio/locapic) 使用

## 适用平台

目前仅支持`Windows (64-bit)`，对`Linux`的支持已在计划中，对`Android`的支持仍在试验中

## 如何使用

### 建立项目

从虚幻引擎建立你的项目`<ProjectName>.uproject`，位于路径`<ProjectDir>`。

### 下载插件

```shell
git clone https://gitlab.com/locapidio/byembed.git <Byembed>
```

克隆源代码到项目插件目录下`<Byembed>`=`<ProjectDir>/Plugins/Byembed`

找到目录`<locapic>`=`<Byembed>/Source/locapic`

### 编译项目

1. 更新项目文件，右键`<ProjectName>.uproject`选择`Generate Visual Studio project files`
2. 编译项目
3. 从`<locapic>/bin/`复制文件`python39.dll` `zlib1.dll`到`<Byembed>/Binaries/Win64`
4. 编辑器启动`<ProjectName>.uproject`，内容浏览器`Byembed/Python`路径下，有一个最简化的关卡`Main`可作为初始参考

| 设置项     | 值                    |
|:--------|:---------------------|
| 默认游戏模式  | CGameModeBase        |
| 编辑器开始地图 | /Byembed/Python/Main |
| 游戏默认地图  | /Byembed/Python/Main |

程序正确运行，必须确保：
* `PYTHONHOME`路径下找到`python.exe`，以正确启动Python环境
* `PYTHONPATH`路径下找到`locapip`目录，且
* `PYTHONHOME`已安装所有`locapip`依赖库，以正确导入`locapip`

`PYTHONHOME`设置为打包生成版本的二进制目录，
原因是`<ProjectName>.exe`和`python.exe`有共同的动态链接库

### 安装或升级`locapip`依赖库

打包生成版本 

* 编辑器内选择`文件`/`打包项目`/`Windows (64-bit)`，选择存放路径`<WindowsNoEditor>`
* 找到目录`PYTHONHOME`=`<WindowsNoEditor>/<ProjectName>/Binaries/Win64`

复制初始环境

* 从目录`<locapic>/bin`复制所有子项到目录`PYTHONHOME`
* 注意，不推荐使用自定义的Python环境，原因是`locapic`已经使用了特定版本的嵌入式CPython

如果使用开发版本的`locapip`，必须自行配置依赖库

```shell
python.exe -m pip install -U click requests grpcio-tools humanize
```

如果使用发布版本的`locapip`

```shell
python.exe -m pip install -U locapip
```

### 设置`PYTHONHOME`和`PYTHONPATH`

打开`Main`关卡蓝图，找到`BeginPython`函数

如何设置`PythonHome`变量，取决于项目处于开发阶段还是发布阶段

* 开发阶段，设置为`PYTHONHOME`的绝对路径
* 发布阶段，设置为空，即`<ProjectName>.exe`所在目录的相对路径

如何设置`PythonPath`，取决于项目依赖的`locapip`是开发版本还是发布版本

* 使用开发版本的`locapip`
  * 设置为开发目录，将导入此目录下的`locapip`
* 使用发布版本的`locapip`
  * 设置为空，将导入`pip`安装的`locapip`

### FAQ

* 为什么只能运行（Play）而无法启动（Launch）？
  * 原因是无法找到动态链接库，解决办法是添加`PYTHONHOME`的绝对路径到设备的`PATH`系统环境变量